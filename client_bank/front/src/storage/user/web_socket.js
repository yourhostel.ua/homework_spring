import { Client } from '@stomp/stompjs';
import SockJS from 'sockjs-client';
import { setPublisherMessage, updateAccountInAccounts } from "./slice";
import store from "../index";
import { useDispatch } from 'react-redux';

const setupWebSocketConnection = (dispatch) => {
    let socket = new SockJS('http://localhost:9000/ws');
    let stompClient = new Client({
        webSocketFactory: () => socket
    });

    stompClient.onConnect = (frame) => {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/accountChange', message => {
            let update = JSON.parse(message.body);
            dispatch(setPublisherMessage(update));
            dispatch(updateAccountInAccounts(update));
            console.log(update);
        });
    };

    stompClient.onStompError = (frame) => {
        console.error('Broker reported error: ' + frame.headers['message']);
        console.error('Additional details: ' + frame.body);
    };

    stompClient.activate();

    return stompClient;
}

export {
    setupWebSocketConnection,
}






