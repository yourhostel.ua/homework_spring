import {createSlice} from '@reduxjs/toolkit';
import builders from '../builders';
import defaultState from "./defaultState";
import {deleteCustomer, loadUser, updateCustomer} from "../asyncThunks";

const user = createSlice({
    name: 'user',
    initialState: defaultState,
    reducers: {
        setPublisherMessage: (state, action) => {
            state.publisher = action.payload;
        },
            updateAccountInAccounts: (state, action) => {
              const updatedAccount = action.payload;
              const accountId = updatedAccount.id;
              state.load.obj.accounts = state.load.obj.accounts.map((account) =>
                account.id === accountId ? { ...account, ...updatedAccount } : account
              );
            },
    },
    extraReducers: builder => {
        builders(builder, loadUser, 'load');
        builders(builder, updateCustomer, 'update');
        builders(builder, deleteCustomer, 'delete');
    }
})

export const {
updateAccountInAccounts,
    setPublisherMessage,
} = user.actions;

export default user.reducer;