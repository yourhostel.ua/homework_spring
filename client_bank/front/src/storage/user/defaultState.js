const defaultInitialState = {
    load: {
        obj: {},
        status: '',
        err: '',
    },
    delete: {
        obj: {},
        status: '',
        err: '',
    },
    update: {
        obj: {},
        status: '',
        err: '',
        editForm: {
            name: '',
            email: '',
            age: '',
            customerId: null,
        },
    },
    publisher:{}
}

export default defaultInitialState