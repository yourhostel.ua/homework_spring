import axios from 'axios';
import { createContext, useContext } from 'react';
//базовий домен для будівництва посилань запитів
const basicAx = axios.create({
    baseURL: 'http://localhost:9000/',
});

const getTokenFromState = (state) => {
    const {
        customers: {
            token: {
                obj: {
                    token: actualToken
                }
            }
        }
    } = state;
    return actualToken || null;
}

//обгортка для асинхронних функцій
const workAx = async (method, url, data, state) => { //
    const token = getTokenFromState(state);
    return basicAx({
        method: method,
        url: url,
        data: data,
        headers: token ? {Authorization: `Bearer ${token}`} : {}
    });
}

//змінна для інтерцептора
export let currentInterceptorId;
//встановлення токена в інтерцептор хедеру
const appInterceptor = (token, component) =>{
    if (currentInterceptorId !== undefined) {
        basicAx.interceptors.request.eject(currentInterceptorId);
    }
    currentInterceptorId = basicAx.interceptors.request.use(
        async (config) => {
                config.headers['Authorization'] = `Bearer ${token}`;
                console.log(`token is set to interceptor in ${component}`, token)
            return config;
        },
        (error) => {
            return Promise.reject(error);
        }
    );
}

const defaultValue = {
    axiosInstance: basicAx,
    axiosInterceptor: appInterceptor
};

const AxiosContext = createContext(defaultValue);

const useAxios = () => {
    return useContext(AxiosContext);
};

const AxiosProvider = ({ children }) => {
    return (
        <AxiosContext.Provider value={defaultValue}>
            {children}
        </AxiosContext.Provider>
    );
};

export {
    getTokenFromState,
    appInterceptor,
    workAx,
    basicAx,
    AxiosProvider,
    useAxios,
}