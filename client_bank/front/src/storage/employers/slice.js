import {createSlice} from '@reduxjs/toolkit';
import builders from '../builders';
import {loadEmployers, loadEmployersByCustomersId} from '../asyncThunks';
import defaultState from "./defaultState";

const employers = createSlice({
    name: 'employers',
    initialState: defaultState,
    reducers: {
        setCurrentPage: (state, action) => {
            state.currentPage = action.payload;
        },
    },
    extraReducers: builder => {
        builders(builder, loadEmployers, 'all');
        builders(builder, loadEmployersByCustomersId, 'byCustomersId');
    }
})

export const {
    setCurrentPage
} = employers.actions;

export default employers.reducer;