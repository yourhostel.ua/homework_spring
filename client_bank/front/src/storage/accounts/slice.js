import {createSlice} from '@reduxjs/toolkit';
import builders from '../builders';
import {loadAccounts, loadAccountsByCustomersId} from '../asyncThunks';
import defaultState from "./defaultState";

const accounts = createSlice({
    name: 'accounts',
    initialState: defaultState,
    reducers: {
        setCurrentPage: (state, action) => {
            state.currentPage = action.payload;
        },
    },
    extraReducers: builder => {
        builders(builder, loadAccounts, 'all');
        builders(builder, loadAccountsByCustomersId, 'byCustomersId');
    }
})

export const {
    setCurrentPage
} = accounts.actions;

export default accounts.reducer;