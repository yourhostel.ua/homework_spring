const defaultInitialState = {
    all: {
        obj: {
            content: [],
            pageable: {
                pageSize: 5,
            },
            totalPages: 0,
        },
        status: '',
        err: '',
    },
    one: {
        obj: {},
        status: '',
        err: '',
    },
    byCustomersId:{
        obj: [],
        status: '',
        err: '',
    },
    currentPage: 0,
}

export default defaultInitialState