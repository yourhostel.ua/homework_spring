import{configureStore,  combineReducers} from "@reduxjs/toolkit";
import {persistReducer, persistStore} from "redux-persist";
import storage from 'redux-persist/lib/storage';
import mainReducer from './slice';
import accountsReducer from './accounts/slice';
import customersReducer from './customers/slice';
import employersReducer from './employers/slice';
import userReducer from './user/slice';
import { basicAx, currentInterceptorId } from './auth/ax';
//змінна для скидання стейту до дефолтного стану
const RESET_STATE = 'RESET_STATE';


const rootReducer = (state, action) => {
    if (action.type === RESET_STATE) {
        return {
            main: undefined,
            accounts: undefined,
            customers: undefined,
            employers: undefined,
            user: undefined,
        };
    }
    return combineReducers({
        main: mainReducer,
        accounts: accountsReducer,
        customers: customersReducer,
        employers: employersReducer,
        user: userReducer,
    })(state, action);
}

const persistConfig = {
    key: 'root',
    storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({serializableCheck: false}),
});

export const persist = persistStore(store);
//функція logout-у
export const resetApp = async ()=> {
    store.dispatch({ type: RESET_STATE }); //скидання редукс стану
   await persist.purge(); //скидання персистентного стану
    if (currentInterceptorId !== undefined) { //скидання інтерцептора axios
        basicAx.interceptors.request.eject(currentInterceptorId);
        currentInterceptorId = undefined;
    }
}

// export const setupStore = preloadedState => { //Тільки для обгортання тесту fetch
//     return configureStore({
//         reducer:  rootReducer,
//         preloadedState,
//     })
// }

export default store;