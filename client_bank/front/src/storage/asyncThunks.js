import {createAsyncThunk} from "@reduxjs/toolkit";
import {basicAx, workAx, getTokenFromState} from "./auth/ax";

/**
 * customers
 */
const auth = createAsyncThunk(
    'customers/token',
    async ({login, password}, {rejectWithValue}) => {
        try {
            const response = await basicAx.post('auth/token', {
                name: login,
                password: password
            });
            return response.data;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

const loadCustomers = createAsyncThunk(
    'customers/all',
    async ({page, size}, {getState, rejectWithValue}) => {
        try {
            const state = getState();
            const response = await workAx('get', `api/customers/page?page=${page}&size=${size}`, null, state);
            console.log("Inside loadCustomers, token is installed", getTokenFromState(state))
            return response.data;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

const updateCustomer = createAsyncThunk(
    'customers/one',
    async ({_id, updatedData} , {getState, rejectWithValue }) => {
        try {
            const response = await workAx('put',`api/customers/update/${_id}`, updatedData, getState());
            return  response.data;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

const deleteCustomer = createAsyncThunk(
    'customers/delete',
    async ({_id, updatedData} , {getState, rejectWithValue }) => {
        try {
            const response = await workAx('delete',`api/customers/${_id}`,null, getState());
            return  response.data;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

/**
 * accounts
 */

const loadAccounts = createAsyncThunk(
    'accounts/all',
    async ({page, size}, {getState, rejectWithValue}) => {
        try {
            const response = await workAx('get',`api/accounts/page?page=${page}&size=${size}`, null, getState());
            return response.data;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

const loadAccountsByCustomersId = createAsyncThunk(
    'accounts/byCustomersId',
    async ({_id}, {getState, rejectWithValue}) => {
        try {
            const response = await workAx('get', `api/accounts/by_customer_id/${_id}`, null, getState());
            return response.data;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

/**
 * employers
 */

const loadEmployers = createAsyncThunk(
    'employers/all',
    async ({page, size}, {getState, rejectWithValue}) => {
        try {
            const response = await workAx('get',`api/employers/page?page=${page}&size=${size}`,null, getState());
            return response.data;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

const loadEmployersByCustomersId = createAsyncThunk(
    'employers/byCustomersId',
    async ({_id}, {getState, rejectWithValue}) => {
        try {
            const response = await workAx('get',`api/employers/by_customer_id/${_id}`, null, getState());
            return response.data;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

/**
 * user
 */
const loadUser = createAsyncThunk(
    'user/load',
    async ({id}, {getState, rejectWithValue}) => {
        try {
            const response = await workAx('get',`api/user/${id}`,null, getState());
            return response.data;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

export {
    loadUser,
    auth,
    loadEmployersByCustomersId,
    loadAccountsByCustomersId,
    loadAccounts,
    loadCustomers,
    updateCustomer,
    loadEmployers,
    deleteCustomer
}