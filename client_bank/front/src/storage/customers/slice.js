import {createSlice} from '@reduxjs/toolkit';
import builders from '../builders';
import {deleteCustomer, loadCustomers, updateCustomer, auth} from '../asyncThunks';
import defaultState from "./defaultState";

const customers = createSlice({
    name: 'customers',
    initialState: defaultState,
    reducers: {
        updateCustomerInList: (state, action) => {
            const {_id, updatedCustomerData} = action.payload;
            state.all.obj.content = state.all.obj.content.map((customer) =>
                customer.id === _id ? {...customer, ...updatedCustomerData} : customer
            );
        },
        deleteCustomerInList: (state, action) => {
            const {_id} = action.payload;
            console.log(_id)
            state.all.obj.content = state.all.obj.content.filter(
                (customer) => customer.id !== _id
            );
        },
        setUpdatedName: (state, action) => {
            state.editForm.name = action.payload;
        },
        setUpdatedEmail: (state, action) => {
            state.editForm.email = action.payload;
        },
        setUpdatedAge: (state, action) => {
            state.editForm.age = action.payload;
        },
        clearEditFormData: (state) => {
            state.editForm = {
                name: '',
                email: '',
                age: '',
                customerId: null,
            };
        },
        setCurrentPage: (state, action) => {
            state.currentPage = action.payload;
        },
    },
    extraReducers: builder => {
        builders(builder, loadCustomers, 'all');
        builders(builder, updateCustomer, 'one');
        builders(builder, deleteCustomer, 'delete');
        builders(builder, auth, 'token');
    }
})

export const {
    deleteCustomerInList,
    updateCustomerInList,
    setCurrentPage,
    clearEditFormData,
    setUpdatedName,
    setUpdatedEmail,
    setUpdatedAge
} = customers.actions;

export default customers.reducer;