const defaultInitialState = {
    all: {
        obj: {
            content: [],
            pageable: {
                pageSize: 10,
            },
            totalPages: 0,
            number: 0,
            size: 0,
            totalElements: 0
        },
        status: '',
        err: '',
    },
    one: {
        obj: {},
        status: '',
        err: '',
    },
    delete: {
        obj: {},
        status: '',
        err: '',
    },
    editForm: {
        name: '',
        email: '',
        age: '',
        customerId: null,
    },
    currentPage: 0,
    token: {
        obj: {},
        status: '',
        err: ''
    },
}

export default defaultInitialState