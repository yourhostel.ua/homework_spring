import {createSlice} from '@reduxjs/toolkit';

const main = createSlice({
    name: 'main',
    initialState: {
        basicModal:{
            isOpen: false,
            showModal: false,
            modalComponent: null,
        },
    },
    reducers: {
        openModal: (state, action) => {
            state.basicModal.isOpen = true;
            state.basicModal.modalComponent = action.payload;
        },
        closeModal: (state) => {
            state.basicModal.isOpen = false;
            state.basicModal.modalComponent = null;
        },
    },
})

export const {
    openModal,
    closeModal,
} = main.actions;

export default main.reducer;