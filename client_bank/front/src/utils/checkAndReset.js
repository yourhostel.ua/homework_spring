import {resetApp} from "../storage";
const isEmptyObject = (obj) => Object.keys(obj).length === 0;
const getNestedObjectValue = (obj, path) => {
    return path.split('.').reduce((acc, segment) => acc && acc[segment], obj);
}
const checkAndReset = (callBack, component, navigate) => {

    const stateData = getNestedObjectValue(callBack, component + ".all");
    if (!stateData) return;

    const {obj, status} = stateData;
    const token = getNestedObjectValue(callBack, "customers.token.obj")

    if ((status === 'fulfilled' && typeof obj === 'string') || isEmptyObject(token)) {
        resetApp().then(_ => {
            console.log(`Reset app in ${component}`)
            navigate("/users/login");
        });
    }
}

export {
    checkAndReset,
}