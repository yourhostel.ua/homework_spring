import * as React from 'react';
import {Route, Routes} from "react-router-dom";
import Chart from "./components/pages/adm/Chart";
import Accounts from "./components/pages/adm/Accounts";
import Deposits from "./components/pages/adm/Deposits";
import Customers from "./components/pages/adm/Customers";
import BasicModal from "./components/fragments/BasicModal";
import Employers from "./components/pages/adm/Employers";
import MoreAccounts from "./components/pages/adm/MoreAccounts";
import MoreEmployers from "./components/pages/adm/MoreEmployers";
import SignInSide from "./components/pages/adm/login";
import Default from "./components/Layout/Default";
import Admin from "./components/Layout/Admin";
import Main from "./components/pages/adm/Main";
import User from "./components/Layout/User";
import UserFacade from "./components/pages/user/UserFacade";

const App = () => {
    return (
        <React.Fragment>
            <BasicModal/>
            <Routes>
                <Route path="/users/login" element={<SignInSide/>}>
                </Route>
                <Route path="/" element={<Default/>}>
                    <Route index element={<Main/>}/>
                </Route>
                <Route path="/user" element={<User/>}>
                    <Route index element={<UserFacade/>}/>
                    <Route path="home" element={<Main/>}/>
                </Route>
                <Route path="/adm" element={<Admin/>}>
                    <Route index element={<Customers/>}/>
                    <Route path="home" element={<Main/>}/>
                    <Route path="user" element={<UserFacade/>}/>
                    <Route path="accounts" element={<Accounts/>}/>
                    <Route path="employers" element={<Employers/>}/>
                    <Route path="more_accounts" element={<MoreAccounts/>}/>
                    <Route path="more_employers" element={<MoreEmployers/>}/>
                    <Route path="chart" element={<Chart/>}/>
                    <Route path="deposits" element={<Deposits/>}/>
                </Route>
            </Routes>
        </React.Fragment>
    )
}

export default App