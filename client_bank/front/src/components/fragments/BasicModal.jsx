import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import {useDispatch, useSelector} from "react-redux";
import { closeModal } from "../../storage/slice";

const styleContainer = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 2,
};

const styleFormBox = {
    p:2,//внутренний отступ
    mt:2,//отступ сверху
    mb:1,//отступ снизу
};
export default function BasicModal() {
    const dispatch = useDispatch();
    const {isOpen, modalComponent} = useSelector((state) => state.main.basicModal);

    const handleClose = () => {
        dispatch(closeModal());
    };

    return (
        <div>
            <Modal
                open={isOpen}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={styleContainer}>
                    <Box sx={styleFormBox}>
                        {modalComponent}
                    </Box>
                </Box>
            </Modal>
        </div>
    );
}