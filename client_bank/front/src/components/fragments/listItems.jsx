import * as React from 'react';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import PeopleIcon from '@mui/icons-material/People';
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import BarChartIcon from '@mui/icons-material/BarChart';
import LayersIcon from '@mui/icons-material/Layers';
import AssignmentIcon from '@mui/icons-material/Assignment';
import {AccountBalanceWallet} from "@mui/icons-material";
import AssuredWorkloadSharpIcon from '@mui/icons-material/AssuredWorkloadSharp';
import {useNavigate} from "react-router-dom";
import {useSelector} from "react-redux";
import {appInterceptor} from "../../storage/auth/ax";
import {resetApp} from "../../storage";
import HomeIcon from '@mui/icons-material/Home';
import LogoutIcon from '@mui/icons-material/Logout';
const handleItemClick = (link, token, navigate) => {
    appInterceptor(token, "listItems");
    navigate(link);

};

const handleReset = async (navigate) => {
   await resetApp();
   navigate("/");
};

export const AdmListItems = () => {
    const navigate = useNavigate();
    const token = useSelector(state => state.customers.token.obj?.token);
    if(!token) navigate("/");
    appInterceptor(token, "listItems");
    return (
        <React.Fragment>
            <ListItemButton onClick={_ => handleItemClick("/adm/home", token, navigate)}>
                <ListItemIcon>
                    <HomeIcon/>
                </ListItemIcon>
                <ListItemText primary="Home"/>
            </ListItemButton>

            <ListItemButton onClick={_ => handleItemClick("/adm", token, navigate)}>
                <ListItemIcon>
                    <PeopleIcon/>
                </ListItemIcon>
                <ListItemText primary="Customers"/>
            </ListItemButton>

            <ListItemButton onClick={_ => handleItemClick("/adm/accounts", token, navigate)}>
                <ListItemIcon>
                    <AccountBalanceWallet/>
                </ListItemIcon>
                <ListItemText primary="Accounts"/>
            </ListItemButton>

            <ListItemButton onClick={_ => handleItemClick("/adm/employers", token, navigate)}>
                <ListItemIcon>
                    <AssuredWorkloadSharpIcon/>
                </ListItemIcon>
                <ListItemText primary="Employers"/>
            </ListItemButton>

            <ListItemButton onClick={_ => handleItemClick("/adm/user", token, navigate)}>
                <ListItemIcon>
                    <AccountBoxIcon/>
                </ListItemIcon>
                <ListItemText primary="My profile"/>
            </ListItemButton>

            <ListItemButton onClick={_ => handleItemClick("/adm/chart", token, navigate)}>
                <ListItemIcon>
                    <BarChartIcon/>
                </ListItemIcon>
                <ListItemText primary="Chart"/>
            </ListItemButton>

            <ListItemButton onClick={_ => handleItemClick("/adm/deposits", token, navigate)}>
                <ListItemIcon>
                    <LayersIcon/>
                </ListItemIcon>
                <ListItemText primary="Deposits"/>
            </ListItemButton>

            <ListItemButton onClick={_=>handleReset(navigate)}>
                <ListItemIcon>
                    <LogoutIcon/>
                </ListItemIcon>
                <ListItemText primary="Logout"/>
            </ListItemButton>

        </React.Fragment>)
};

export const UserListItems = () => {
    const navigate = useNavigate();
    const token = useSelector(state => state.customers.token.obj?.token)
    if(!token) navigate("/");
    appInterceptor(token, "listItems");
    return (
        <React.Fragment>

            <ListItemButton onClick={_ => handleItemClick("/user/home", token, navigate)}>
                <ListItemIcon>
                    <HomeIcon/>
                </ListItemIcon>
                <ListItemText primary="Home"/>
            </ListItemButton>

            <ListItemButton onClick={_ => handleItemClick("/user", token, navigate)}>
                <ListItemIcon>
                    <AccountBoxIcon/>
                </ListItemIcon>
                <ListItemText primary="My profile"/>
            </ListItemButton>

            <ListItemButton onClick={_=>handleReset(navigate)}>
                <ListItemIcon>
                    <LogoutIcon/>
                </ListItemIcon>
                <ListItemText primary="Logout"/>
            </ListItemButton>

        </React.Fragment>
    );
}

export const defaultListItems = (
    <React.Fragment>
        <ListItemButton to={"users/login"}>
            <ListItemIcon>
                <PeopleIcon/>
            </ListItemIcon>
            <ListItemText primary="Log in"/>
        </ListItemButton>

        <ListItemButton to={"users/signup"}>
            <ListItemIcon>
                <AccountBalanceWallet/>
            </ListItemIcon>
            <ListItemText primary="Sign up"/>
        </ListItemButton>
    </React.Fragment>
);

export const secondaryListItems = (
    <React.Fragment>
        <ListSubheader component="div" inset>
            Saved reports
        </ListSubheader>
        <ListItemButton>
            <ListItemIcon>
                <AssignmentIcon/>
            </ListItemIcon>
            <ListItemText primary="Current month"/>
        </ListItemButton>
        <ListItemButton>
            <ListItemIcon>
                <AssignmentIcon/>
            </ListItemIcon>
            <ListItemText primary="Last quarter"/>
        </ListItemButton>
        <ListItemButton>
            <ListItemIcon>
                <AssignmentIcon/>
            </ListItemIcon>
            <ListItemText primary="Year-end sale"/>
        </ListItemButton>
    </React.Fragment>
);