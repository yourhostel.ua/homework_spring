import * as React from 'react';
import Layout from "./index";
import {UserListItems} from "../fragments/listItems";
import Copyright from "../fragments/Copyright";
import Telling from "../fragments/Telling";

const User = () => {
    return (
        <React.Fragment>
            <Layout titleChapter={"User"}
                    mainListItems={<UserListItems/>}
                    copyright={Copyright}
                    telling={Telling}/>
        </React.Fragment>
    )
}

export default User