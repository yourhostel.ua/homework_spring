import * as React from 'react';
import Layout from "./index";
import {AdmListItems, secondaryListItems} from "../fragments/listItems";
import Copyright from "../fragments/Copyright";
import Telling from "../fragments/Telling";

const Admin = () => {
    return (
        <React.Fragment>
        <Layout titleChapter={"Admin"}
                mainListItems={<AdmListItems/>}
                secondaryListItems={secondaryListItems}
                copyright={Copyright}
                telling={Telling}/>
    </React.Fragment>
    )
}

export default Admin