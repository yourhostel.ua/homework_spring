import * as React from 'react';
import Layout from "./index";
import {defaultListItems} from "../fragments/listItems";

const Default = () => {
    return (
        <React.Fragment>
            <Layout titleChapter={"Com Bank"} mainListItems={defaultListItems}></Layout>
        </React.Fragment>
    )
}

export default Default