import {useDispatch} from 'react-redux';
import {closeModal} from "../../storage/slice";
import {Button, Box} from '@mui/material';
import {deleteCustomer} from "../../storage/asyncThunks";
import Typography from "@mui/material/Typography";
import {deleteCustomerInList} from "../../storage/customers/slice";

const DeleteFormCustomers = ({_id, title}) => {
    const dispatch = useDispatch();
    const handleDelete = async () => {
        try {
            await dispatch(deleteCustomer({_id}));
            dispatch(deleteCustomerInList({ _id }));
            dispatch(closeModal());
        } catch (error) {
            console.error('error DeleteCustomer in DeleteForm', error);
        }
    };

    const handleClose = () => {
        dispatch(closeModal());
    };

    return (
        <div>
            <Box sx={{p: 2}}>
                <Typography _id="modal-modal-title" variant="h5" component="h5" sx={{mb: 3}}>
                    {title}
                </Typography>
                {"Object will be deleted  id: " + _id}
                <Box sx={{display: 'flex', justifyContent: 'space-evenly', alignItems: 'center', mt: 5}}>
                    <Button variant="contained" color="primary" onClick={handleDelete}>
                        Ok
                    </Button>
                    <Box sx={{ml: 2}}>
                        <Button variant="contained" color="secondary" onClick={handleClose}>
                            Cancel
                        </Button>
                    </Box>
                </Box>
            </Box>
        </div>
    );
};

export default DeleteFormCustomers;