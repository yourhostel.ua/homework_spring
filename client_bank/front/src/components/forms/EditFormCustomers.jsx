import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {closeModal} from "../../storage/slice";
import { TextField, Button, Box } from '@mui/material';
import { updateCustomer } from "../../storage/asyncThunks";
import { clearEditFormData, setUpdatedName,
  setUpdatedAge, setUpdatedEmail,
  updateCustomerInList } from '../../storage/customers/slice';
import Typography from "@mui/material/Typography";

const EditFormCustomers = ({ defaultName, defaultEmail, defaultAge, _id, title }) => {
  const dispatch = useDispatch();
  const { name, email, age } = useSelector((state) => state.customers.editForm);

  useEffect(() => {
    dispatch(setUpdatedName(defaultName));
    dispatch(setUpdatedEmail(defaultEmail));
    dispatch(setUpdatedAge(defaultAge));
  }, [defaultName, defaultEmail, defaultAge, dispatch]);

  const handleUpdate = async () => {
    const updatedCustomerData = {
      name,
      email,
      age,
    };

    try {
      await dispatch(updateCustomer({_id, updatedData: updatedCustomerData}));
      dispatch(updateCustomerInList({_id, updatedCustomerData: updatedCustomerData}));
      dispatch(closeModal());
    } catch (error) {
      console.error('error updateCustomer in EditForm', error);
    }
  };

  const handleClose = () => {
    dispatch(clearEditFormData());
    dispatch(closeModal());
  };

  return (
      <React.Fragment>
        <Typography _id="modal-modal-title" variant="h5" component="h5" sx={{mb: 3}}>
          {title}
        </Typography>
        {"id: " + _id}
        <Box sx={{ p: 2 }}>
          <TextField
              id="standard-basic"
              label="Name"
              variant="standard"
              value={name}
              onChange={(e) => dispatch(setUpdatedName(e.target.value))}
          />
          <TextField
              id="standard-basic"
              label="Email"
              variant="standard"
              value={email}
              onChange={(e) => dispatch(setUpdatedEmail(e.target.value))}
          />
          <TextField
              id="standard-basic"
              label="Age"
              variant="standard"
              value={age}
              onChange={(e) => dispatch(setUpdatedAge(e.target.value))}
          />
          <Box sx={{ display: 'flex', justifyContent: 'space-evenly', alignItems: 'center', mt: 5 }}>
            <Button variant="contained" color="primary" onClick={handleUpdate}>
              Save
            </Button>
            <Box sx={{ ml: 2 }}>
              <Button variant="contained" color="secondary" onClick={handleClose}>
                Cancel
              </Button>
            </Box>
          </Box>
        </Box>
      </React.Fragment>
  );
};

export default EditFormCustomers;