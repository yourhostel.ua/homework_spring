import * as React from 'react';
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Title from "../../fragments/Title";
import {LinearProgress} from "@mui/material";
import Table from "@mui/material/Table";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import {useDispatch, useSelector} from "react-redux";
import ModeEditOutlineOutlinedIcon from "@mui/icons-material/ModeEditOutlineOutlined";
import IconButton from "@mui/material/IconButton";
import AddToPhotosOutlinedIcon from "@mui/icons-material/AddToPhotosOutlined";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import Box from "@mui/material/Box";
import {loadUser} from "../../../storage/asyncThunks";
import {setupWebSocketConnection} from "../../../storage/user/web_socket";
import {useEffect} from "react";
import TableHead from "@mui/material/TableHead";
import {Bar, BarChart, CartesianGrid, XAxis, YAxis} from "recharts";
import {useNavigate} from "react-router-dom";

const UserFacade = () => {
    const dispatch = useDispatch();
    useEffect(() => {
            setupWebSocketConnection(dispatch);
    }, []);
    const {status} = useSelector(state => state.user.load)
    const {accounts = [], employers = [], ...profile} = useSelector(state => state.user.load.obj || {})
    const {token} = useSelector(state => state.customers.token.obj);
    const navigate = useNavigate();

    const {id} = useSelector(state => state.customers.token.obj)

    const data = accounts.map(account => ({
        name: account.currency,
        data1: account.balance,
        data2: account.balance * 0.7
    }));

    useEffect(() => {
        dispatch(loadUser({id: id}));
    }, [dispatch]);

    useEffect(() => {
        if (!token) {
            navigate('/');
        }
    }, [token, navigate]);

    return (
        <React.Fragment>
            <Grid item xs={12}>
                <Grid container rowSpacing={1} columnSpacing={{xs: 1, sm: 2, md: 3}}>
                    <Grid item xs={12} sm={6}>
                        <Paper sx={{p: 2, display: 'flex', flexDirection: 'column', minHeight: '300px'}}>
                            <Box style={{display: 'flex', justifyContent: 'space-between'}}>
                                <Title>Profile</Title>
                                <Box display="flex">
                                    <IconButton>
                                        <ModeEditOutlineOutlinedIcon sx={{'&:hover': {color: 'green'}}}/>
                                    </IconButton>
                                    <IconButton>
                                        <AddToPhotosOutlinedIcon sx={{'&:hover': {color: 'green'}}}/>
                                    </IconButton>
                                    <IconButton>
                                        <DeleteForeverIcon sx={{'&:hover': {color: 'red'}}}/>
                                    </IconButton>
                                </Box>
                            </Box>
                            {status === "pending" ?
                                (<LinearProgress/>) : (<Table size="small">
                                    <TableBody>
                                        <TableRow>
                                            <TableCell>{"Name"}</TableCell>
                                            <TableCell>{profile.name}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>{"Created date"}</TableCell>
                                            <TableCell>{profile.created_date}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>{"Modified date"}</TableCell>
                                            <TableCell>{profile.last_modified_date}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>{"Tel"}</TableCell>
                                            <TableCell>{profile.tel}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>{"Email"}</TableCell>
                                            <TableCell>{profile.email}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>{"Age"}</TableCell>
                                            <TableCell>{profile.age}</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>)}
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Paper sx={{p: 2, minHeight: '300px'}}>
                            <Title>Accounts</Title>
                            {status === "pending" ?
                                (<LinearProgress/>) : (<Table size="small">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>{"Id"}</TableCell>
                                            <TableCell>{"Currency"}</TableCell>
                                            <TableCell>{"Balance"}</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {accounts.map(account => (
                                            <TableRow>
                                                <TableCell>{account.id}</TableCell>
                                                <TableCell>{account.currency}</TableCell>
                                                <TableCell>{account.balance}</TableCell>
                                            </TableRow>))}
                                    </TableBody>
                                </Table>)}
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Paper sx={{p: 2, minHeight: '300px'}}>
                            <Title>Employers</Title>
                            {status === "pending" ?
                                (<LinearProgress/>) : (<Table size="small">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>{"Id"}</TableCell>
                                            <TableCell>{"Currency"}</TableCell>
                                            <TableCell>{"Balance"}</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {employers.map(employer => (
                                            <TableRow>
                                                <TableCell>{employer.id}</TableCell>
                                                <TableCell>{employer.name}</TableCell>
                                                <TableCell>{employer.address}</TableCell>
                                            </TableRow>))}
                                    </TableBody>
                                </Table>)}
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Paper sx={{p: 2, minHeight: '300px'}}>
                            <Title>Other</Title>
                            <BarChart width={500} height={300} data={data}>
                                <CartesianGrid strokeDasharray="3 3"/>
                                <XAxis dataKey="name"/>
                                <YAxis/>
                                <Bar dataKey="data1" fill="#8884d8"/>
                                <Bar dataKey="data2" fill="#82ca9d"/>
                            </BarChart>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}

export default UserFacade