import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Title from '../../fragments/Title';
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";

import {LinearProgress} from "@mui/material";
import {useSelector} from "react-redux";
import {useLocation, useNavigate} from "react-router-dom";
import {useEffect} from "react";

const MoreEmployers = () => {
    const navigate = useNavigate();
    const { token } = useSelector(state => state.customers.token.obj);
    useEffect(() => {if (!token) {navigate('/');}}, [token, navigate]);
    const { obj, status } = useSelector((state) => state.employers.byCustomersId);
    const title = useLocation().state
    return (
        <React.Fragment>
            <Grid item xs={12}>
                <Paper sx={{p: 2, display: 'flex', flexDirection: 'column'}}>
                    <Title>{title}</Title>
                    {status === "pending" ?
                        (<LinearProgress/>) : (<Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Id</TableCell>
                                    <TableCell>Name</TableCell>
                                    <TableCell align="left">Address</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {obj?.map((employer) => (
                                    <TableRow key={employer.id}>
                                        <TableCell>{employer.id}</TableCell>
                                        <TableCell>{employer.name}</TableCell>
                                        <TableCell align="left">{employer.address}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>)}
                </Paper>
            </Grid>
        </React.Fragment>
    );
};

export default MoreEmployers;