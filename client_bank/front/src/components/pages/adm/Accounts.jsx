import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Title from '../../fragments/Title';
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import {useDispatch, useSelector} from "react-redux";
import {loadAccounts} from '../../../storage/asyncThunks';
import {useEffect} from "react";
import {LinearProgress, Pagination} from "@mui/material";
import {setCurrentPage} from "../../../storage/accounts/slice";

const Accounts = () => {
    const dispatch = useDispatch();

    const {
        all: {
            obj: {
                content,
                pageable: {
                    pageSize
                },
                totalPages,
            },
            status
        },
        currentPage,
    } = useSelector((state) => state.accounts);

    setCurrentPage(0)

    if (content === undefined) {
        dispatch(loadAccounts({page: 0, size: 5}));
    }

    useEffect(() => {
        dispatch(loadAccounts({page: currentPage, size: pageSize}));
    }, [dispatch, currentPage, pageSize]);

    const handleChangePage = (event, newPage) => {
        dispatch(setCurrentPage(newPage - 1));
    };

    return (
        <React.Fragment>
            <Grid item xs={12}>
                <Paper sx={{p: 2, display: 'flex', flexDirection: 'column'}}>
                    <Title>Recent Accounts</Title>
                    {status === "pending" ?
                        (<LinearProgress/>) : (<Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Id</TableCell>
                                    <TableCell>Number</TableCell>
                                    <TableCell>Currency</TableCell>
                                    <TableCell>Balance</TableCell>
                                    <TableCell align="right">Customer</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {content?.map((account) => (
                                    <TableRow key={account.id}>
                                        <TableCell>{account.id}</TableCell>
                                        <TableCell>{account.number}</TableCell>
                                        <TableCell>{account.currency}</TableCell>
                                        <TableCell>{account.balance}</TableCell>
                                        <TableCell align="right">{account.customer.name}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>)}
                    <Pagination
                        count={totalPages}
                        page={currentPage + 1}
                        onChange={handleChangePage}
                    />
                </Paper>
            </Grid>
        </React.Fragment>
    );
};

export default Accounts;