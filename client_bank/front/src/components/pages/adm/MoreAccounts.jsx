import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Title from '../../fragments/Title';
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";

import {LinearProgress} from "@mui/material";
import {useSelector} from "react-redux";
import {useLocation, useNavigate} from "react-router-dom";
import {useEffect} from "react";

const MoreAccounts = () => {
    const { obj, status } = useSelector(state => state.accounts.byCustomersId);
    const { token } = useSelector(state => state.customers.token.obj);
    const title = useLocation().state;
    const navigate = useNavigate();
    useEffect(() => {if (!token) {navigate('/');}}, [token, navigate]);
    return (
        <React.Fragment>
            <Grid item xs={12}>
                <Paper sx={{p: 2, display: 'flex', flexDirection: 'column'}}>
                    <Title>{title}</Title>
                    {status === "pending" ?
                        (<LinearProgress/>) : (<Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Id</TableCell>
                                    <TableCell>Number</TableCell>
                                    <TableCell>Currency</TableCell>
                                    <TableCell>Balance</TableCell>
                                    <TableCell align="right">Customer</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {obj?.map((account) => (
                                    <TableRow key={account.id}>
                                        <TableCell>{account.id}</TableCell>
                                        <TableCell>{account.number}</TableCell>
                                        <TableCell>{account.currency}</TableCell>
                                        <TableCell>{account.balance}</TableCell>
                                        <TableCell align="right">{account.customer.name}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>)}
                </Paper>
            </Grid>
        </React.Fragment>
    );
};

export default MoreAccounts;