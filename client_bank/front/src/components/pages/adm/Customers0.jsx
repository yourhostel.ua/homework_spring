import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Close';
import {
    GridRowModes,
    DataGrid,
    GridToolbarContainer,
    GridActionsCellItem,
    GridRowEditStopReasons,
} from '@mui/x-data-grid';
import {useDispatch, useSelector} from "react-redux";
import {loadCustomers, updateAccount} from "../../../storage/asyncThunks";
import {useEffect, useState} from "react";
import {LinearProgress} from "@mui/material";
import Title from "../../fragments/Title";
import Grid from "@mui/material/Grid";

export default function FullFeaturedCrudGrid() {
    const dispatch = useDispatch();

    const {
        obj: {
            content,
            pageable: {
                pageSize
            },
            // totalPages,
            // number,
            size,
            totalElements
        },
        status
    } = useSelector((state) => state.my.customers);

    function EditToolbar(props) {
        const {setRows, setRowModesModel} = props;

        const handleClick = () => {
            const id = content?.length + 1;
            setRows((oldRows) => [...oldRows, {id, name: '', age: '', isNew: true}]);
            setRowModesModel((oldModel) => ({
                ...oldModel,
                [id]: {mode: GridRowModes.Edit, fieldToFocus: 'name'},
            }));
        };

        return (
            <GridToolbarContainer>
                <Button color="primary" startIcon={<AddIcon/>} onClick={handleClick}>
                    Add record
                </Button>
            </GridToolbarContainer>
        );
    }

    //********************** Page *******************************

    const [currentPage, setCurrentPage] = useState(0);
    const customersPerPage = pageSize;

    useEffect(() => {

        dispatch(loadCustomers({page: currentPage, size: customersPerPage}));
    }, [dispatch, currentPage, customersPerPage]);

    useEffect(() => {
        setRows(content);
    }, [content, currentPage, customersPerPage]);

    const handleChangePage = (event, newPage) => {
        setCurrentPage(newPage - 1);
    };
    //********************** end ********************************

    const [rows, setRows] = React.useState(content);
    const [rowModesModel, setRowModesModel] = React.useState({});

    // const handleRowEditStop = (params, event) => {
    //     if (params.reason === GridRowEditStopReasons.rowFocusOut) {
    //         event.defaultMuiPrevented = true;
    //     }
    // };
    const handleRowEditStop = async (params, event) => {
        console.log(params)
        if (params.reason === GridRowEditStopReasons.rowFocusOut) {
            event.defaultMuiPrevented = true;
        }

        if (params.id) {
            try {
                const updatedRow = params.props.row;
                await dispatch(updateAccount({ accountId: updatedRow.id, updatedAccount: updatedRow }));
            } catch (error) {

            }
        }
    };

    const handleEditClick = (id) => () => {
        setRowModesModel({...rowModesModel, [id]: {mode: GridRowModes.Edit}});
    };

    const handleSaveClick = (id) => () => {
        setRowModesModel({...rowModesModel, [id]: {mode: GridRowModes.View}});
    };

    const handleDeleteClick = (id) => () => {
        setRows(rows.filter((row) => row.id !== id));
    };

    const handleCancelClick = (id) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: {mode: GridRowModes.View, ignoreModifications: true},
        });

        const editedRow = rows.find((row) => row.id === id);
        if (editedRow.isNew) {
            setRows(rows.filter((row) => row.id !== id));
        }
    };

    const processRowUpdate = (newRow) => {
        const updatedRow = {...newRow, isNew: false};
        setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
        return updatedRow;
    };

    const handleRowModesModelChange = (newRowModesModel) => {
        setRowModesModel(newRowModesModel);
    };

    const columns = [
        {
            field: 'id',
            headerName: 'Id',
            width: 100,
            editable: false
        },
        {
            field: 'name',
            headerName: 'Name',
            width: 180,
            editable: true
        },
        {
            field: 'email',
            headerName: 'Email',
            width: 180,
            editable: true,
        },
        {
            field: 'age',
            headerName: 'Age',
            type: 'number',
            width: 80,
            align: 'left',
            headerAlign: 'left',
            editable: true,
        },
        {
            field: 'account',
            headerName: 'Account',
            width: 220,
        },
        {
            field: 'actions',
            type: 'actions',
            headerName: 'Actions',
            width: 100,
            // align: 'right', // Выравнивание по правому краю (только pro)
            // headerAlign: 'right', // Выравнивание заголовка по правому краю (только pro)
            cellClassName: 'actions',
            getActions: ({id}) => {
                const isInEditMode = rowModesModel[id]?.mode === GridRowModes.Edit;

                if (isInEditMode) {
                    return [
                        <GridActionsCellItem
                            icon={<SaveIcon/>}
                            label="Save"
                            sx={{
                                color: 'primary.main',
                            }}
                            onClick={handleSaveClick(id)}
                        />,
                        <GridActionsCellItem
                            icon={<CancelIcon/>}
                            label="Cancel"
                            className="textPrimary"
                            onClick={handleCancelClick(id)}
                            color="inherit"
                        />,
                    ];
                }

                return [
                    <GridActionsCellItem
                        icon={<EditIcon/>}
                        label="Edit"
                        className="textPrimary"
                        onClick={handleEditClick(id)}
                        color="inherit"
                    />,
                    <GridActionsCellItem
                        icon={<DeleteIcon/>}
                        label="Delete"
                        onClick={handleDeleteClick(id)}
                        color="inherit"
                    />,
                ];
            },
        },
    ];

    return (
        <Grid item xs={12}>
            <Title>Recent Customers</Title>
            <Box
                sx={{
                    height: 500,
                    width: '100%',
                    '& .actions': {
                        color: 'text.secondary',
                    },
                    '& .textPrimary': {
                        color: 'text.primary',
                    },
                }}
            >

                {status === "pending" ?
                    (<LinearProgress/>) : (
                        <DataGrid
                            rows={rows ? rows : []}
                            columns={columns}
                            editMode="row"
                            rowModesModel={rowModesModel}
                            onRowModesModelChange={handleRowModesModelChange}
                            onRowEditStop={handleRowEditStop}
                            processRowUpdate={processRowUpdate}
                            slots={{
                                toolbar: EditToolbar,
                            }}
                            slotProps={{
                                toolbar: {setRows, setRowModesModel},
                            }}
                            //pagination={false} // только версия pro
                            pagination
                            pageSize={size}//Количество строк на одной странице
                            rowCount={totalElements} // Общее количество элементов (строк) c выше 100 только версия pro
                            page={currentPage + 1} // Текущая страница
                            autoPageSize={false}
                            paginationMode="client" // Установить режим серверной пагинации
                            onPageChange={handleChangePage}
                            //paginationPerPageOptions={[5, 10, 25, 50, 100]} только версия pro
                        />)}
            </Box>
        </Grid>
    );
}