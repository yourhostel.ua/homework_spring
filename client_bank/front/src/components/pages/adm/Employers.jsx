import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Title from '../../fragments/Title';
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import {useDispatch, useSelector} from "react-redux";
import {loadEmployers} from '../../../storage/asyncThunks';
import {useEffect} from "react";
import {LinearProgress, Pagination} from "@mui/material";
import {setCurrentPage} from "../../../storage/employers/slice";

const Employers = () => {
    const dispatch = useDispatch();

    const {
        all:{
            obj: {
                content,
                pageable: {
                    pageSize
                },
                totalPages,
            },
            status
        },
        currentPage,
    } = useSelector((state) => state.employers);

    if (content === undefined) {
        dispatch(loadEmployers({page: 0, size: 5}));
    }

    useEffect(() => {
        dispatch(loadEmployers({page: currentPage, size: pageSize}));
    }, [dispatch, currentPage, pageSize]);

    const handleChangePage = (event, newPage) => {
        dispatch(setCurrentPage(newPage - 1));
    };

    return (
        <React.Fragment>
            <Grid item xs={12}>
                <Paper sx={{p: 2, display: 'flex', flexDirection: 'column'}}>
                    <Title>Recent Employers</Title>
                    {status === "pending" ?
                        (<LinearProgress/>) : (<Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Id</TableCell>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Address</TableCell>
                                    <TableCell align="right">Customers</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {content?.map((employer) => (
                                    <TableRow key={employer.id}>
                                        <TableCell>{employer.id}</TableCell>
                                        <TableCell>{employer.name}</TableCell>
                                        <TableCell>{employer.address}</TableCell>
                                        <TableCell align="right">{employer.customers.length}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>)}
                    <Pagination
                        count={totalPages}
                        page={currentPage + 1}
                        onChange={handleChangePage}
                    />
                </Paper>
            </Grid>
        </React.Fragment>
    );
};

export default Employers;