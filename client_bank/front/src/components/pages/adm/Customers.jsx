import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import AddToPhotosOutlinedIcon from '@mui/icons-material/AddToPhotosOutlined';
import ModeEditOutlineOutlinedIcon from '@mui/icons-material/ModeEditOutlineOutlined';
import Title from '../../fragments/Title';
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import {useDispatch, useSelector} from "react-redux";
import EditFormCustomers from "../../forms/EditFormCustomers";
import {loadAccountsByCustomersId, loadEmployersByCustomersId, loadCustomers} from "../../../storage/asyncThunks";
import {useEffect} from "react";
import {LinearProgress, Pagination} from "@mui/material";
import {openModal} from "../../../storage/slice";
import {setCurrentPage} from "../../../storage/customers/slice";
import IconButton from "@mui/material/IconButton";
import Box from "@mui/material/Box";
import DeleteFormCustomers from "../../forms/DeleteFormCustomers";
import {useNavigate} from "react-router-dom";
import {checkAndReset} from "../../../utils/checkAndReset";

const Customers = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { customers, ...restState } = useSelector((state) => state);

    checkAndReset(restState, "customers", navigate);

    const {
        all: {
            obj: {
                content,
                pageable: {
                    pageSize
                },
                totalPages,
            },
            status,
        },
        currentPage,
    } = customers;

    const handleOpen = (modalComponent) => {
        dispatch(openModal(modalComponent));
    };

    const moreAccounts = (_id, name) => {
        dispatch(loadAccountsByCustomersId({_id}));
        navigate(`/adm/more_accounts`, {state: `Accounts: ${name}'s`});
    }

    const moreEmployers = (_id, name) => {
        dispatch(loadEmployersByCustomersId({_id}));
        navigate(`/adm/more_employers`, {state: `Employers: ${name}'s`});
    }

    setCurrentPage(0)

    if (content === undefined) {
        dispatch(loadCustomers({page: 0, size: 10}));
    }

    useEffect(() => {
        dispatch(loadCustomers({page: currentPage, size: pageSize}));
    }, [dispatch, currentPage, pageSize]);

    const handleChangePage = (event, newPage) => {
        dispatch(setCurrentPage(newPage - 1));
    };

    return (
        <React.Fragment>
            <Grid item xs={12}>
                <Paper sx={{p: 2, display: 'flex', flexDirection: 'column'}}>
                    <Title>Recent Customers</Title>
                    {status === "pending" ?
                        (<LinearProgress/>) : (<Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell>id</TableCell>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Email</TableCell>
                                    <TableCell align="center">Age</TableCell>
                                    <TableCell align="center">Accounts</TableCell>
                                    <TableCell align="center">Employers</TableCell>
                                    <TableCell align="right">Actions</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {content?.map((customer) => (
                                    <TableRow key={customer.id}>
                                        <TableCell>{customer.id}</TableCell>
                                        <TableCell>{customer.name}</TableCell>
                                        <TableCell>{customer.email}</TableCell>
                                        <TableCell align="center">
                                            {customer.age}
                                        </TableCell>
                                        <TableCell>
                                            <Box sx={{display: 'flex', justifyContent: 'center'}}>
                                                <IconButton
                                                    onClick={() => {
                                                        moreAccounts(customer.id, customer.name)
                                                    }}
                                                    sx={{'&:hover': {color: 'gold',},}}>
                                                    {customer.accounts.length}
                                                </IconButton>
                                            </Box>
                                        </TableCell>
                                        <TableCell>
                                            <Box sx={{display: 'flex', justifyContent: 'center'}}>
                                                <IconButton
                                                    onClick={() => {
                                                        moreEmployers(customer.id, customer.name)
                                                    }}
                                                    sx={{'&:hover': {color: 'gold',},}}>
                                                    {customer.employers.length}
                                                </IconButton>
                                            </Box>
                                        </TableCell>
                                        <TableCell>
                                            <Box sx={{display: 'flex', justifyContent: 'flex-end'}}>
                                                <IconButton
                                                    onClick={() => handleOpen(<EditFormCustomers
                                                        title={"Make changes?"}
                                                        _id={customer.id}
                                                        defaultName={customer.name}
                                                        defaultEmail={customer.email}
                                                        defaultAge={customer.age}
                                                    />)}>
                                                    <ModeEditOutlineOutlinedIcon sx={{'&:hover': {color: 'green',},}}/>
                                                </IconButton>

                                                <IconButton>
                                                    <AddToPhotosOutlinedIcon sx={{'&:hover': {color: 'green',},}}/>
                                                </IconButton>

                                                <IconButton
                                                    onClick={() => handleOpen(<DeleteFormCustomers
                                                        title={"Are you sure?"}
                                                        _id={customer.id}
                                                    />)}>
                                                    <DeleteForeverIcon sx={{'&:hover': {color: 'red',},}}/>
                                                </IconButton>
                                            </Box>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>)}
                    <Pagination
                        count={totalPages}
                        page={currentPage + 1}
                        onChange={handleChangePage}
                    />
                </Paper>
            </Grid>
        </React.Fragment>
    );
}

export default Customers;