import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {Provider} from 'react-redux';
import store from './storage';
import {PersistGate} from "redux-persist/integration/react";
import { useAxios, AxiosProvider } from './storage/auth/ax';
import {persist} from "./storage";
const root = ReactDOM.createRoot(document.getElementById('root'));

const Run = () => {
    return (
        <React.StrictMode>
            <Provider store={store}>
                <BrowserRouter>
                    <PersistGate loading={null} persistor={persist}>
                        <AxiosProvider instance={useAxios()}>
                            <App />
                        </AxiosProvider>
                    </PersistGate>
                </BrowserRouter>
            </Provider>
        </React.StrictMode>
    );
};

root.render(<Run />);
