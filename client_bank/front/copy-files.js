const fs = require('fs-extra');
const path = require('path');

const buildPath = path.resolve(__dirname, 'build');
const staticPath = path.resolve(__dirname, '../src/main/resources/static');

//Видалення папки static (якщо вона існує)
fs.removeSync(staticPath);

// Копіювання вмісту папки build/static в папку src/main/resources/static
fs.copySync(path.join(buildPath, 'static'), staticPath);

// Переміщення вмісту папки build/image до папки src/main/resources/static/image
const buildImgPath = path.join(buildPath, 'image');
const staticImgPath = path.join(staticPath, 'image');
if (fs.existsSync(buildImgPath)) {
  fs.moveSync(buildImgPath, staticImgPath);
}

// Копіювання всіх файлів з папки build (крім папок) до папки src/main/resources/static
fs.readdirSync(buildPath).forEach((file) => {
  const filePath = path.join(buildPath, file);
  const stat = fs.statSync(filePath);
  if (stat.isFile()) {
    fs.copySync(filePath, path.join(staticPath, file));
  }
});

console.log('Файли React app скопійовані в src/main/resources/');