package com.bank.repository;

import com.bank.model.Employer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployerRepository extends JpaRepository<Employer, Long> {
    void deleteAllByCustomersIsEmpty();
}
