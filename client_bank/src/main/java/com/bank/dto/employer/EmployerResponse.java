package com.bank.dto.employer;

import com.bank.model.Customer;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
@Data
public class EmployerResponse {
    private Long id;
    private LocalDateTime created_date;
    private LocalDateTime last_modified_date;
    String name;
    String address;
    private List<Customer> customers;
}
