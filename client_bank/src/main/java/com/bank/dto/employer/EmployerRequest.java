package com.bank.dto.employer;

import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class EmployerRequest {
    @Size(min = 2, message = "The name must contain at least 3 characters")
    String name;
    @Size(min = 2, message = "The address must contain at least 3 characters")
    String address;
}
