package com.bank.dto.employer;

import lombok.Data;

@Data
public class EmployerIdDTO {
    private Long id;
}
