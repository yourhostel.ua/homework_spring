package com.bank.dto.employer;

import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
public class EmployerMergeRequest {
    @NotNull(message = "The field must not be NULL")
    @Positive(message = "Employer ID must be a positive number")
    @Digits(integer = 10, fraction = 0, message = "Employer ID must be a valid number")
    private Long employerId;
    @NotNull(message = "The field must not be NULL")
    @Positive(message = "Employer ID must be a positive number")
    @Digits(integer = 10, fraction = 0, message = "Employer ID must be a valid number")
    private Long customerId;
}
