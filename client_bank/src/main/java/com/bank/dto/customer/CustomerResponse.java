package com.bank.dto.customer;

import com.bank.dto.account.AccountIdDTO;
import com.bank.dto.employer.EmployerIdDTO;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class CustomerResponse{
    private Long id;
    private LocalDateTime created_date;
    private LocalDateTime last_modified_date;
    private String name;
    private String email;
    private String tel;
    private Integer age;
    private List<AccountIdDTO> accounts;
    private List<EmployerIdDTO> employers;
}
