package com.bank.dto.customer;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomerRequest {
        @Size(min = 2, message = "The name must contain at least 2 characters")
        private String name;

        @NotNull
        @Email(regexp = "^[\\w.-]+@[\\w.-]+\\.[A-Za-z]{2,}$", message = "Incorrect email format")
        private String email;

        @Pattern(regexp = "^\\+38\\(0\\d{2}\\)\\d{7}$",
                message = "Incorrect phone number format. Should be +38(0XX)XXXXXXX")
        private String tel;

        @Min(value = 18, message = "The user must be at least 18 years old")
        private Integer age;

        @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%~*?&])[A-Za-z\\d@$!~%*?&]{7,}$",
                message = """
            Password must contain at least 7 characters, one uppercase letter,
            one lowercase letter, and one special character
            """)
        private String password;
}
