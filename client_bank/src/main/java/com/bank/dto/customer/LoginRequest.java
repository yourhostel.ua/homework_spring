package com.bank.dto.customer;

public record LoginRequest(String name, String password, Boolean rememberMe) {}
