package com.bank.dto.account;

import com.bank.model.Currency;
import com.bank.model.Customer;
import lombok.Data;

import java.time.LocalDateTime;
@Data
public class AccountResponse{
    private Long id;
    private LocalDateTime created_date;
    private LocalDateTime last_modified_date;
    private String number;
    private Currency currency;
    private Double balance;
    private Customer customer;
}
