package com.bank.dto.account;

import com.bank.model.Currency;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountRequest {
    @NotNull(message = "Currency must not be null")
    private Currency currency;
    @PositiveOrZero
    private Double balance;
}
