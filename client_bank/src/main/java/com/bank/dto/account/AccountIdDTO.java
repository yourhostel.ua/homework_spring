package com.bank.dto.account;

import lombok.Data;

@Data
public class AccountIdDTO {
    private Long id;
}
