package com.bank.dto.account;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AccountPublisher {
    private Long id;
    private LocalDateTime created_date;
    private LocalDateTime last_modified_date;
    private String number;
    private Double balance;
    private String customerName;
    private String notificationMessage;
}
