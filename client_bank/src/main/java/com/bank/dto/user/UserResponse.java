package com.bank.dto.user;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class UserResponse {
    private Long id;
    private LocalDateTime created_date;
    private LocalDateTime last_modified_date;
    private String name;
    private String email;
    private String tel;
    private Integer age;
    private List<AccountForUser> accounts;
    private List<EmployerForUser> employers;
}

