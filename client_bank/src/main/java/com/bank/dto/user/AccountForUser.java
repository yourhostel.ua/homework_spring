package com.bank.dto.user;

import com.bank.model.Currency;
import lombok.Data;

import java.time.LocalDateTime;
@Data
public class AccountForUser {
    private Long id;
    private LocalDateTime created_date;
    private LocalDateTime last_modified_date;
    private String number;
    private Currency currency;
    private Double balance;
}
