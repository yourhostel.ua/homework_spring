package com.bank.dto.user;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class EmployerForUser {
    private Long id;
    private LocalDateTime created_date;
    private LocalDateTime last_modified_date;
    String name;
    String address;
}
