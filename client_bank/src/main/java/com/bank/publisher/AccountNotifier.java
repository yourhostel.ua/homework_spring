package com.bank.publisher;

import com.bank.dto.account.AccountPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class AccountNotifier {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    public void notifyAccountChange(AccountPublisher account) {
        messagingTemplate.convertAndSend("/topic/accountChange", account);
    }
}
