package com.bank.controller.user;

import com.bank.dto.user.UserResponse;
import com.bank.facade.UserFacade;
import com.bank.model.Customer;
import com.bank.service.CustomerService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

@Validated
@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class ApiUser {
    private final CustomerService service;
    private final UserFacade facade;

    @GetMapping(value = "user/{id}", produces = "application/json")
    public UserResponse getUser(@PathVariable("id") Long id) {
        Customer customer = service.getOne(id);
        return Optional.ofNullable(customer)
                        .map(facade::convertToUserResponse)
                .orElseThrow(() -> new EntityNotFoundException("User"));
    }
}
