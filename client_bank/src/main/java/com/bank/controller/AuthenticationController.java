package com.bank.controller;

import com.bank.config.security.JwtTokenService;
import com.bank.dto.customer.LoginRequest;
import com.bank.dto.customer.*;
import com.bank.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RestController
@RequestMapping("auth")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000", methods = {RequestMethod.POST})
public class AuthenticationController {

    private final CustomerService service;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenService tokenService;

    @PostMapping("token")
    public ResponseEntity<LoginResponse> handleLogin(@RequestBody LoginRequest rq) {
        return service.findByName(rq.name())
                .filter(u -> passwordEncoder.matches(rq.password(), u.getPassword()))
                .map(u -> LoginResponse.Ok(u.getId(), tokenService.generateToken(Math.toIntExact(u.getId())), u.getRoles()))
                .map(loginResponseOk -> {
                    log.info("AuthenticationController: Token sent!");
                    return ResponseEntity.ok(loginResponseOk);
                })
                .orElseGet(() -> {
                    log.error("AuthenticationController: User not found or wrong user/password combination");
                    return ResponseEntity
                            .status(HttpStatus.FORBIDDEN)
                            .body(LoginResponse.Error("wrong user/password combination"));
                });
    }
}