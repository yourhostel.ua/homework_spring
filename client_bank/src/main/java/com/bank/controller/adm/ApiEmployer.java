package com.bank.controller.adm;

import com.bank.dto.employer.EmployerMergeRequest;
import com.bank.dto.employer.EmployerRequest;
import com.bank.dto.employer.EmployerResponse;
import com.bank.config.exception.customException.CreationException;
import com.bank.config.exception.customException.MergeException;
import com.bank.facade.EmployerFacade;
import com.bank.model.Employer;
import com.bank.service.EmployerService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class ApiEmployer {
    private final EmployerService service;
    private final EmployerFacade facade;
    @PersistenceContext
    private EntityManager em;

    @GetMapping(value = "employers", produces = "application/json")
    public List<EmployerResponse> getAllEmployer() {
        List<Employer> allEmployer = service.findAll();
        return allEmployer.stream()
                .map(facade::convertToEmployerResponse)
                .collect(Collectors.toList());
    }

    @PostMapping(value = "employers/create{idCustomers}", consumes = "application/json")
    public ResponseEntity<EmployerResponse> createEmployer(@Valid @RequestBody EmployerRequest employerRequest,
                                                           @PathVariable("idCustomers") Long idCustomers) {
        return Optional.of(facade.convertToEmployer(employerRequest))
                .map(employer->service.save(employer, idCustomers))
                .map(facade::convertToEmployerResponse)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new CreationException("Failed to create employer"));
    }

    @PostMapping(value = "employers/merge/{employerId}/{customerId}", consumes = "application/json")
    public ResponseEntity<EmployerResponse> mergeEmployerAndCustomerByURL(
            @RequestParam @NotNull @Positive Long employerId,
            @RequestParam @NotNull @Positive Long customerId) {

        return Optional.of(service.mergeEmployerAndCustomer(employerId, customerId))
                .map(facade::convertToEmployerResponse)
                .map(ResponseEntity::ok)
                .orElseThrow(()->new MergeException("Failed to merge employer and customer", "EMPLOYER_CUSTOMER_MERGE_FAILED"));
    }

    @PostMapping(value = "employers/merge", consumes = "application/json")
    public ResponseEntity<EmployerResponse> mergeEmployerAndCustomerByBody(
            @Valid @RequestBody EmployerMergeRequest mergeRequest) {

        return Optional.of(mergeRequest)
                .map(request -> service.mergeEmployerAndCustomer(request.getEmployerId(), request.getCustomerId()))
                .map(facade::convertToEmployerResponse)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new MergeException("Failed to merge employer and customer", "EMPLOYER_CUSTOMER_MERGE_FAILED"));
    }

    @GetMapping(value = "employers/page", produces = "application/json")
    public Page<EmployerResponse> getEmployerByPage(@RequestParam(value = "page", defaultValue = "0") int page,
                                            @RequestParam(value = "size", defaultValue = "5") int size) {
        return service.getByPage(PageRequest.of(page, size))
                .map(facade::convertToEmployerResponse);
    }

    @GetMapping("employers/by_customer_id/{customer_id}")
    public List<EmployerResponse> findEmployersByCustomerId(@PathVariable("customer_id") Long customer_id) {
        String queryStr = "SELECT e FROM Employer e JOIN e.customers c WHERE c.id = :customer_id";
        TypedQuery<Employer> query = em.createQuery(queryStr, Employer.class);
        query.setParameter("customer_id", customer_id);
        return query.getResultList()
                .stream()
                .map(facade::convertToEmployerResponse)
                .collect(Collectors.toList());
    }
}
