package com.bank.controller.adm;

import com.bank.dto.customer.CustomerRequest;
import com.bank.dto.customer.CustomerResponse;
import com.bank.config.exception.customException.CreationException;
import com.bank.facade.CustomerFacade;
import com.bank.model.Customer;
import com.bank.service.CustomerService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Validated
@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class ApiCustomer {
    private final CustomerService service;
    private final CustomerFacade facade;

    @GetMapping(value = "customers", produces = "application/json")
    public List<CustomerResponse> getAllCustomers() {
        List<Customer> allCustomers = service.findAll();
        return Optional.ofNullable(allCustomers)
                .map(customers -> customers.stream()
                        .map(facade::convertToCustomerResponse)
                        .collect(Collectors.toList()))
                .orElseThrow(() -> new EntityNotFoundException("No customers found"));
    }

    @PostMapping(value = "customers/create", consumes = "application/json")
    public ResponseEntity<CustomerResponse> createCustomer(@Valid @RequestBody CustomerRequest customerRequest) {
        return Optional.of(facade.convertToCustomer(customerRequest))
                .map(service::save)
                .map(facade::convertToCustomerResponse)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new CreationException("Failed to create customer"));
    }

    @GetMapping(value = "customers/{id}", produces = "application/json")
    public ResponseEntity<CustomerResponse> getCustomer(@PathVariable("id") Long id) {
        return service.findById(id)
                .map(facade::convertToCustomerResponse)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new EntityNotFoundException("Customer with ID " + id));
    }

    @PutMapping("customers/update/{id}")
    public ResponseEntity<CustomerResponse> updateCustomer(@PathVariable("id") Long id,
                                                           @Valid @RequestBody CustomerRequest customerRequest) {
        return service.findById(id)
                .map(existingCustomer -> {
                    facade.updateToCustomer(existingCustomer, customerRequest);
                    Customer updatedCustomer = service.save(existingCustomer);
                    CustomerResponse customerResponse = facade.convertToCustomerResponse(updatedCustomer);
                    return ResponseEntity.ok(customerResponse);
                })
                .orElseThrow(() -> new EntityNotFoundException("Customer with ID " + id));
    }

    @DeleteMapping("customers/{id}")
    public ResponseEntity<String> deleteCustomer(@PathVariable("id") Long id) {
        return Optional.of(service.deleteById(id))
                .filter(deleted->deleted)
                .map(delete->ResponseEntity.ok(String.format("Customer ID %s deleted successfully", id)))
                .orElseThrow(()->new EntityNotFoundException("Customer with ID " + id));
    }

    @CrossOrigin(origins = "http://localhost:3000", methods = {RequestMethod.GET})
    @GetMapping(value = "customers/page", produces = "application/json")
    public Page<CustomerResponse> getCustomersByPage(@RequestParam(value = "page", defaultValue = "0") int page,
                                                     @RequestParam(value = "size", defaultValue = "10") int size) {
        Page<Customer> customersPage = service.getByPage(PageRequest.of(page, size));
        return Optional.ofNullable(customersPage)
                .map(customers -> customers.map(facade::convertToCustomerResponse))
                .orElseThrow(() -> new EntityNotFoundException("No Page customers found"));
    }
}
