package com.bank.controller.adm;

import com.bank.dto.account.AccountRequest;
import com.bank.dto.account.AccountResponse;
import com.bank.facade.AccountFacade;
import com.bank.model.Account;
import com.bank.service.AccountService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class ApiAccount {
    private final AccountService service;
    private final AccountFacade facade;

    @GetMapping("accounts")
    public List<Account> getAllAccounts() {
        return service.findAll();
    }

    @PostMapping("accounts/create/{customerId}")
    public ResponseEntity<AccountResponse> createAccountForCustomer(@Valid @RequestBody AccountRequest accountRequest,
                                                             @PathVariable("customerId") Long customerId) {
        return facade.createNewAccount(accountRequest)
                .map(account -> service.saveAccountForExistingCustomer(account, customerId))
                .map(facade::convertToAccountResponse)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new EntityNotFoundException("Account with ID " + customerId));
    }

    @GetMapping(value = "accounts/{id}", produces = "application/json")
    public ResponseEntity<AccountResponse> getAccount(@PathVariable("id") Long id) {
        return service.findById(id)
                .map(facade::convertToAccountResponse)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new EntityNotFoundException("Account with ID " + id));
    }

    @PutMapping("accounts/update/{id}")
    public ResponseEntity<AccountResponse> updateAccount(@PathVariable("id") Long id,
                                                 @Valid @RequestBody AccountRequest accountRequest) {
        return service.findById(id)
                .map(account -> {
                    account.setBalance(accountRequest.getBalance());
                    account.setCurrency(accountRequest.getCurrency());
                    return service.save(account);
                })
                .map(facade::convertToAccountResponse)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new EntityNotFoundException("Account with ID " + id));
    }

    @DeleteMapping("accounts/{id}")
    public ResponseEntity<String> deleteAccount(@PathVariable("id") Long id) {
        return Optional.of(service.deleteById(id))
                .filter(deleted -> deleted)
                .map(deleted -> ResponseEntity.ok("Account deleted successfully"))
                .orElseThrow(() -> new EntityNotFoundException("Account with ID " + id));
    }

    @GetMapping(value = "accounts/page", produces = "application/json")
    public Page<AccountResponse> getAccountsByPage(@RequestParam(value = "page", defaultValue = "0") int page,
                                           @RequestParam(value = "size", defaultValue = "5") int size) {
        return service.getByPage(PageRequest.of(page, size))
                .map(facade::convertToAccountResponse);
    }

    @GetMapping("accounts/by_customer_id/{customer_id}")
    public List<AccountResponse> accountsByCustomerId(@PathVariable("customer_id") Long customerId) {
                return service.accountsByCustomerId(customerId).stream()
                .map(facade::convertToAccountResponse)
                .collect(Collectors.toList());
    }
}

