package com.bank.utils;

import com.bank.controller.adm.ApiAccount;
import com.bank.controller.adm.ApiCustomer;
import com.bank.controller.adm.ApiEmployer;
import com.bank.dto.account.AccountRequest;
import com.bank.dto.customer.CustomerResponse;
import com.bank.dto.employer.EmployerRequest;
import com.bank.facade.EmployerFacade;
import com.bank.model.Customer;
import com.bank.model.Employer;
import com.bank.service.CustomerService;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationContext;
import com.bank.model.Currency;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
@Log4j2
public class Gen {

    public final ApplicationContext context;

    private Gen(ApplicationContext context) {
        this.context = context;
    }

    public static Gen of(ApplicationContext context) {
        return new Gen(context);
    }

    public final List<Customer> customers = List.of(
            new Customer("Greak", "g_rafk@ukr.net", 42),
            new Customer("Bret", "Sincere@april.biz", 19),
            new Customer("Ervin", "Shanna@melissa.tv", 31),
            new Customer("Clementine", "clementine_b@ukr.net", 27),
            new Customer("Patricia", "Nathan@yesenia.net", 58),
            new Customer("Karianne", "Julianne.OConner@kory.org", 29),
            new Customer("Kamren", "Lucio_Hettinger@annie.ca", 19),
            new Customer("Leopoldo_Corkery", "Karley_Dach@jasper.info", 56),
            new Customer("Elwyn.Skiles", "Telly.Hoeger@billy.biz", 45),
            new Customer("Maxime_Nienow", "Sherwood@rosamond.me", 98),
            new Customer("Glenna Reichert", "Chaim_McDermott@dana.io", 39),
            new Customer("Clementina DuBuque", "Rey.Padberg@karina.biz", 51)
    );

    public final List<Employer> employers = List.of(
            new Employer("Toshiba", "Токіо", new ArrayList<>()),
            new Employer("Amazon.com Inc.", "Seattle, Washington and Arlington, Virginia, U.S.", new ArrayList<>()),
            new Employer("Umbrella Corporation", "top secret", new ArrayList<>()),
            new Employer("Cyberdyne Systems", "USA", new ArrayList<>()),
            new Employer("Resources Development Administration", "Silicon Valley California", new ArrayList<>()),
            new Employer("Google", "USA California", new ArrayList<>()),
            new Employer("Weyland-Yutani Corporation", "earth planet", new ArrayList<>()),
            new Employer("Kongō Gumi", "Osaka, Japan", new ArrayList<>()),
            new Employer("KUKA Systems", "Augsburg, Bavaria, Germany", new ArrayList<>()),
            new Employer("Baykar", "Istanbul, Turkey", new ArrayList<>()),
            new Employer("ITER", "Provence-Alpes-Côte d'Azur", new ArrayList<>()),
            new Employer("Roche Holding", "Switzerland", new ArrayList<>())
    );

    public final String[] telPref = new String[]{"039", "067", "068", "096", "097", "098", "050", "066", "095", "099", "063", "073", "093"};
    public final String[] password = new String[]{"568D!6s", "S06i7*378", "Ff063?82@", "ldsf~gb1K", "oEk%jdf57", "0&98ZnSdfg",
            "*g!H235h", "WgfIl63?", "2hdsU56F!", "gdhC%12U", "0Qwezx&63", "0!H7jfd3?", "09jjI3*U"};

    public int random(int min, int max) {
        int range = max - min + 1; // 0 .... max - min + 1
        double rand = Math.random() * range;
        return (int) (rand + min);
    }

    public void genEmployer() {
        ApiEmployer employerController = context.getBean(ApiEmployer.class);
        ApiCustomer customerController = context.getBean(ApiCustomer.class);
        EmployerFacade employerFacade = context.getBean(EmployerFacade.class);

        List<Long> idList = customerController.getAllCustomers().stream()
                .map(CustomerResponse::getId).toList();

        idList.forEach(id -> {
            for (int i = random(0, 4); i > 0; i--) {
                Employer employer = employers.get(random(0, 11));
                EmployerRequest employerRequest = employerFacade.convertToEmployerRequest(employer);
                employerController.createEmployer(employerRequest, id);
            }
        });
    }

    public void genCustomers() {
        CustomerService service = context.getBean(CustomerService.class);
        PasswordEncoder encoder = context.getBean(PasswordEncoder.class);

        for (Customer customer : customers) {

            StringBuilder tel = new StringBuilder();
            for (int j = 0; j < 7; j++) tel.append(random(0, 9));
            customer.setTel(String.format("+38(%s)%s", telPref[random(0, 12)], tel));

            switch (customer.getName()) {
// Унікальний адмін з точно відомим паролем
// login: Greak password: 568D!6s role: "ADMIN", "USER"
                case "Greak" -> {
                    String pAdmin = password[0];
                    customer.setPassword(encoder.encode(pAdmin));
                    customer.setRoles(new String[]{"ADMIN", "USER"});
                    log.info("Створено користувача: Customer{name={}, password={}}\n", customer.getName(), pAdmin);
                }
// Унікальний юзер з точно відомим паролем
// login: Bret password: S06i7*378 role: "USER"
                case "Bret" -> {
                    String pUser = password[1];
                    customer.setPassword(encoder.encode(pUser));
                    customer.setRoles(new String[]{"USER"});
                    log.info("Створено користувача: Customer{name={}, password={}}\n", customer.getName(), pUser);
                }
                default -> { //рандомні customers
                    customer.setRoles(new String[]{"USER"});
                    customer.setPassword(encoder.encode(password[random(0, 12)]));
                }
            }
        }

// Фільтр унеможливлює повторно додати тестових user та admin
// роблячи їх унікальними за будь-якої кількості генерацій
        customers.stream()
                .filter(customer ->
                        !((customer.getName().equals("Greak") || customer.getName().equals("Bret"))
                                && service.findByName(customer.getName()).isPresent()))
                .forEach(service::save);
    }

    public void genAccounts() {
        ApiAccount accountController = context.getBean(ApiAccount.class);
        ApiCustomer customerController = context.getBean(ApiCustomer.class);

        List<Long> idList = customerController.getAllCustomers().stream()
                .map(CustomerResponse::getId).toList();

        Currency[] currency = Currency.values();
        for (Long id : idList) {
            for (int i = random(0, 4); i > 0; i--) {
                accountController.createAccountForCustomer(
                        new AccountRequest(currency[i], (double) random(0, 500000)), id);
            }
        }
    }

    public void clearDB() {
        CustomerService customerService = context.getBean(CustomerService.class);
        List<Long> idList = customerService.findAll().stream()
                .map(Customer::getId).toList();
        for (Long id : idList) customerService.deleteById(id);
    }
}
