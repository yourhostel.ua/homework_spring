package com.bank.facade;

import com.bank.dto.employer.EmployerRequest;
import com.bank.dto.employer.EmployerResponse;
import com.bank.model.Employer;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class EmployerFacade {
    private final ModelMapper modelMapper;
    public EmployerResponse convertToEmployerResponse(Employer employer) {
        EmployerResponse response = modelMapper.map(employer, EmployerResponse.class);
        response.setCreated_date(employer.getCreatedDate());
        response.setLast_modified_date(employer.getLastModifiedDate());
        return response;
    }

    public EmployerRequest convertToEmployerRequest(Employer employer) {
        return modelMapper.map(employer, EmployerRequest.class);
    }

    public Employer convertToEmployer(EmployerRequest employerRequest){
        return modelMapper.map(employerRequest, Employer.class);
    }
}
