package com.bank.facade;

import com.bank.dto.account.AccountPublisher;
import com.bank.dto.account.AccountRequest;
import com.bank.dto.account.AccountResponse;
import com.bank.model.Account;
import com.bank.model.Customer;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class AccountFacade {
    private final ModelMapper modelMapper;

    public AccountResponse convertToAccountResponse(Account account){
        AccountResponse response = modelMapper.map(account, AccountResponse.class);
        response.setCreated_date(account.getCreatedDate());
        response.setLast_modified_date(account.getLastModifiedDate());
        return response;
    }

    public Optional<Account> createNewAccount(AccountRequest accountRequest){
        Account account = new Account();
        account.setCustomer(new Customer());
        account.setCurrency(accountRequest.getCurrency());
        Optional<Double> balanceOptional = Optional.ofNullable(accountRequest.getBalance());
        account.setBalance(balanceOptional.orElse(0.0));
        account.setNumber(UUID.randomUUID().toString());
        return Optional.of(account);
    }

    public AccountPublisher convertToAccountPublisher(Account account) {
        AccountPublisher publisher = modelMapper.map(account, AccountPublisher.class);
        publisher.setCreated_date(account.getCreatedDate());
        publisher.setLast_modified_date(account.getLastModifiedDate());
        return publisher;
    }
}
