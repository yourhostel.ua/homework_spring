package com.bank.facade;

import com.bank.dto.customer.CustomerRequest;
import com.bank.dto.customer.CustomerResponse;
import com.bank.model.Customer;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
@Component
@RequiredArgsConstructor
public class CustomerFacade {
    private final ModelMapper modelMapper;

    public CustomerResponse convertToCustomerResponse(Customer customer) {
        CustomerResponse response = modelMapper.map(customer, CustomerResponse.class);
        response.setCreated_date(customer.getCreatedDate());
        response.setLast_modified_date(customer.getLastModifiedDate());
        return response;
    }

    public CustomerRequest convertToCustomerRequest(Customer customer) {
        return modelMapper.map(customer, CustomerRequest.class);
    }

    public Customer convertToCustomer(CustomerRequest customerRequest) {
        return modelMapper.map(customerRequest, Customer.class);
    }

    public void updateToCustomer(Customer existingCustomer, CustomerRequest customerRequest) {
        existingCustomer.setName(customerRequest.getName());
        existingCustomer.setEmail(customerRequest.getEmail());
        existingCustomer.setAge(customerRequest.getAge());
        existingCustomer.setTel(customerRequest.getTel());
    }
}
