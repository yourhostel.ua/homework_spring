package com.bank.facade;

import com.bank.dto.user.AccountForUser;
import com.bank.dto.user.EmployerForUser;
import com.bank.dto.user.UserResponse;
import com.bank.model.Account;
import com.bank.model.Customer;
import com.bank.model.Employer;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserFacade {
    private final ModelMapper modelMapper;
    public UserResponse convertToUserResponse(Customer customer) {
        UserResponse response = modelMapper.map(customer, UserResponse.class);
        response.setCreated_date(customer.getCreatedDate());
        response.setLast_modified_date(customer.getLastModifiedDate());
        // Конвертація вкладених об'єктів
        response.setAccounts(customer
                .getAccounts()
                .stream()
                .map(this::convertToAccountForUser)
                .collect(Collectors.toList()));
        response.setEmployers(customer
                .getEmployers()
                .stream()
                .map(this::convertToEmployerForUser)
                .collect(Collectors.toList()));

        return response;
    }

    public AccountForUser convertToAccountForUser(Account account) {
        AccountForUser response = modelMapper.map(account, AccountForUser.class);
        response.setCreated_date(account.getCreatedDate());
        response.setLast_modified_date(account.getLastModifiedDate());
        return response;
    }

    public EmployerForUser convertToEmployerForUser(Employer employer) {
        EmployerForUser response = modelMapper.map(employer, EmployerForUser.class);
        response.setCreated_date(employer.getCreatedDate());
        response.setLast_modified_date(employer.getLastModifiedDate());
        return response;
    }
}
