package com.bank.service;

import com.bank.dao.AccountDao;
import com.bank.dto.account.AccountPublisher;
import com.bank.facade.AccountFacade;
import com.bank.publisher.AccountNotifier;
import com.bank.repository.AccountRepository;
import com.bank.model.Account;
import com.bank.repository.CustomerRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AccountService implements AccountDao {
    private final AccountRepository accountRepository;
    private final CustomerRepository customerRepository;
    private final AccountFacade accountFacade;
    private final AccountNotifier accountNotifier;
    @Override
    public Account save(Account account) {
        Account savedAccount = accountRepository.save(account);
        AccountPublisher accountPublisher = accountFacade.convertToAccountPublisher(savedAccount);
        accountNotifier.notifyAccountChange(accountPublisher);
        return savedAccount;
    }

    public Account saveAccountForExistingCustomer(Account account, Long customerId) {
        return customerRepository
                .findById(customerId)
                .map(customer -> {
                    account.setCustomer(customer);
                    return accountRepository.save(account);
                })
                .orElseThrow(() -> new EntityNotFoundException("Customer with ID " + customerId + " not found"));
    }

    @Override
    public boolean delete(Account account) {
        try {
            accountRepository.delete(account);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void deleteAll(List<Account> entities) {
        accountRepository.deleteAll(entities);
    }

    @Override
    public void saveAll(List<Account> entities) {
        accountRepository.saveAll(entities);
    }

    @Override
    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    @Transactional
    @Override
    public boolean deleteById(long id) {
        return accountRepository.findById(id)
                .map(account -> {
                    account
                            .getCustomer()
                            .getAccounts()
                            .remove(account);
                    accountRepository.delete(account);
                    return true;
                })
                .orElse(false);
    }

    @Override
    public Account getOne(long id) {
        return accountRepository.findById(id).orElse(null);
    }

    @Override
    public Page<Account> getByPage(Pageable pageable) {
        return accountRepository.findAll(pageable);
    }

    @Override
    public Optional<Account> findById(Long id){
        return accountRepository.findById(id);
    }

    public List<Account> accountsByCustomerId(Long customerId) {
        return accountRepository.findByCustomer_Id(customerId);
    }
}
