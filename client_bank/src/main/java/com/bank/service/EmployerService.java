package com.bank.service;

import com.bank.dao.EmployerDAO;
import com.bank.model.Customer;
import com.bank.model.Employer;
import com.bank.repository.CustomerRepository;
import com.bank.repository.EmployerRepository;
import com.bank.utils.EX;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployerService implements EmployerDAO {

    private final EmployerRepository employerRepository;
    private final CustomerRepository customerRepository;

    @Override
    public Employer save(Employer employer) {
        throw EX.NI;
    }
    @Transactional
    public Employer save(Employer employer, Long customerId) {
        Optional<Customer> optionalCustomer = customerRepository.findById(customerId);
        if (optionalCustomer.isEmpty()) {
            throw new IllegalArgumentException("Customer with id " + customerId + " not found");
        }

        Customer customer = optionalCustomer.get();
        customer.getEmployers().add(employer);
        employer.getCustomers().add(customer);

        employerRepository.save(employer);

        return employer;
    }

    @Transactional
    public Employer mergeEmployerAndCustomer(Long employerId, Long customerId) {
        Employer employer = employerRepository.findById(employerId)
                .orElseThrow(() -> new EntityNotFoundException("Employer with ID " + employerId + " not found"));

        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new EntityNotFoundException("Customer with ID " + customerId + " not found"));
        //перевіряємо, що зв'язок не існує, і створюємо новий зв'язок,
        // у протилежному випадку повертаємо поточний об'єкт без змін
        return Optional.of(employer)
                .filter(e -> !e.getCustomers().contains(customer))
                .map(e -> {
                    e.getCustomers().add(customer);
                    customer.getEmployers().add(e);
                    return employerRepository.save(e);
                })
                .orElse(employer);
    }

    @Override
    public boolean delete(Employer employer) {
        try {
            employerRepository.delete(employer);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void deleteAll(List<Employer> employers) {
        employerRepository.deleteAll(employers);
    }

    @Override
    public void saveAll(List<Employer> employers) {
        employerRepository.saveAll(employers);
    }

    @Override
    public List<Employer> findAll() {
        return employerRepository.findAll();
    }

    @Override
    public boolean deleteById(long id) {
        try {
            employerRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Employer getOne(long id) {
        return employerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Employer with id " + id + " not found"));
    }

    @Override
    public Optional<Employer> findById(Long id) {
        return employerRepository.findById(id);
    }

    @Override
    public Page<Employer> getByPage(Pageable pageable) {
        return employerRepository.findAll(pageable);
    }
}
