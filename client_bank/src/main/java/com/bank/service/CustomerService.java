package com.bank.service;

import com.bank.dao.CustomerDao;
import com.bank.model.Employer;
import com.bank.repository.CustomerRepository;
import com.bank.model.Customer;
import com.bank.repository.EmployerRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerService implements CustomerDao {
    private final CustomerRepository customerRepository;
    private final EmployerRepository employerRepository;

    @Transactional
    @Override
    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public boolean delete(Customer customer) {
        try {
            customerRepository.delete(customer);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void deleteAll(List<Customer> entities) {
        customerRepository.deleteAll(entities);
    }

    @Override
    public void saveAll(List<Customer> entities) {
        customerRepository.saveAll(entities);
    }

    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    /**
     * Для каскадного видалення employers із залежністю
     * many to many при видаленні Customer:
     *   1.Перевіряємо у нього список employers на наявність незв'язних сутностей
     *   з іншими Customer, якщо такі знаходяться поміщаємо їх у список на видалення
     *     employersToDelete і видаляємо всіх з цього списка.
     *   2.Потім видаляємо самого Customer.
     *   3.І перевіряємо щоб не залишилося "employer-ов без зв'язку"
     *     видаляємо якщо такі залишилися.
     * Таким чином при видаленні Customer каскадно видаляються сутності:
     *     accounts за допомогою механізму JPA - CascadeType.ALL і employers
     *     за допомогою логіки в методі deleteById
     */
    @Transactional
    @Override
    public boolean deleteById(long id) {
        // Знаходимо клієнта за заданим ідентифікатором
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        if (optionalCustomer.isPresent()) {
            Customer customer = optionalCustomer.get();

            // Формуємо список Employer, які потрібно видалити
            // (ті, які належать лише цьому Customer і не мають інших Customer)
            List<Employer> employersToDelete = new ArrayList<>();
            for (Employer employer : customer.getEmployers()) {
                if (employer.getCustomers().size() == 1 && employer.getCustomers().contains(customer)) {
                    employersToDelete.add(employer);
                }
            }

            // Видаляємо employers з бази даних
            employerRepository.deleteAll(employersToDelete);

            // Видаляємо Customer з бази даних
            customerRepository.delete(customer);

            // Видаляємо всі employers, у яких немає зв'язаних Customer
            employerRepository.deleteAllByCustomersIsEmpty();

            return true;
        }
        return false;
    }

    @Override
    public Customer getOne(long id) {
        return customerRepository.findById(id).orElse(null);
    }

    @Override
    public Page<Customer> getByPage(Pageable pageable) {
        return customerRepository.findAll(pageable);
    }

    @Override
    public Optional<Customer> findById(Long id){
        return customerRepository.findById(id);
    }

    public Optional<Customer> findByName(String name){
        return customerRepository.findByName(name);
    }
}