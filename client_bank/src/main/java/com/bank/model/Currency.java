package com.bank.model;

public enum Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP,
}
