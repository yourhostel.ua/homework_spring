package com.bank.model;

import com.fasterxml.jackson.annotation.*;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Data
@Entity
@NoArgsConstructor
@Table(name = "customers")
@EqualsAndHashCode(callSuper = true)
public class Customer extends AbstractEntity{
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String email;
    String tel;
    private Integer age;
    @Column(nullable = false)
    String password;

    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private List<Account> accounts;

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private List<Employer> employers;

    private String roles;
    @Transient
    private final String DELIMITER = ":";
    public void setRoles(String[] roles) {
        this.roles = String.join(DELIMITER, roles);
    }

    public String[] getRoles() {
        return this.roles.split(DELIMITER);
    }

    /**
     * Конструктор використовується виключно для генерації
     */
    public Customer(String name, String email, Integer age) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.accounts = new ArrayList<>();
    }
    @Override
    public String toString() {
        return String.format("Customer{id=%d, Name=%s, Email=%s, Age=%d, Tel=%s, AccountIds=%s, EmployerIds=%s}",
                getId(), name, email, age, tel,
                accounts.stream().map(Account::getId).collect(Collectors.toList()),
                employers.stream().map(Employer::getId).collect(Collectors.toList()));
    }
}
