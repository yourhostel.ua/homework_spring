package com.bank.model;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@Table(name = "accounts")
@EqualsAndHashCode(callSuper = false)
public class Account extends AbstractEntity{
    private String number;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Currency currency;
    private Double balance;
    @ManyToOne
    @JoinColumn(name = "customer_id")
    @JsonIdentityReference(alwaysAsId = true)
    private Customer customer;

    public Account(Currency currency, Double balance, Customer customer) {
        this.number = UUID.randomUUID().toString();
        this.currency = currency;
        this.balance = balance;
        this.customer = customer;
    }

    @Override
    public String toString() {
        return String.format("Account{id=%d, number=%s, currency=%s, balance=%f, customer_id=%d}",
                getId(), number, currency, balance, customer.getId());
    }
}