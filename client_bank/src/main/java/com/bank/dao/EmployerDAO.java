package com.bank.dao;

import com.bank.model.Employer;

public interface EmployerDAO extends DAO<Employer>{
    Employer save(Employer employer, Long customerId);
}
