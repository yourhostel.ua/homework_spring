package com.bank.dao;

import com.bank.model.Customer;

public interface CustomerDao extends DAO<Customer> {

}
