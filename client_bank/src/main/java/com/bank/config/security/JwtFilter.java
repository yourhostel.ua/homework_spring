package com.bank.config.security;

import com.bank.service.CustomerService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Optional;

@Log4j2
@Component
@RequiredArgsConstructor
public class JwtFilter extends OncePerRequestFilter {

    private static final String BEARER = "Bearer ";
    private static final String AUTH_HEADER = HttpHeaders.AUTHORIZATION;
    private final JwtTokenService tokenService;
    private final CustomerService customerService;

    private Optional<String> extractTokenFromRequest(HttpServletRequest rq) {
        return Optional.ofNullable(rq.getHeader(AUTH_HEADER))
                .filter(h -> h.startsWith(BEARER))
                .map(h -> h.substring(BEARER.length()));
    }

    private void processTokenAndSetupAuthentication(HttpServletRequest request, HttpServletResponse response) {
        extractTokenFromRequest(request)
                .flatMap(tokenService::parseToken)
                .map(id -> new JwtCustomerDetails(id, customerService))
                .map(ud -> new UsernamePasswordAuthenticationToken(ud, null, ud.getAuthorities()))
                .ifPresentOrElse(at -> {
                    at.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(at);
                }, () -> log.error("Failed to process setup authentication in filter"));
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain) throws ServletException,
            IOException {
        if (isRequestForProtectedResource(request)) {
            try {
                processTokenAndSetupAuthentication(request, response);
                log.info("Roles: {}",
                        SecurityContextHolder
                                .getContext()
                                .getAuthentication());
                chain.doFilter(request, response);
            } catch (UsernameNotFoundException ex) {
                log.info("UsernameNotFoundException: Username not found; {}", ex.getMessage());
                response.sendRedirect("/users/login");
            }  catch (ServletException | IOException ex) {
                log.error("Servlet error occurred {}", ex.getMessage());
            } catch (Exception ex) {
                log.error("Unexpected error occurred: {}", ex.getMessage());
            }
        } else {
            chain.doFilter(request, response);
        }

    }

    private boolean isRequestForProtectedResource(HttpServletRequest request) {
        String path = request.getRequestURI();
        boolean bool;
        switch (path) {
            case "/users/login", "/auth/token", "/" -> bool = false;
            default -> bool = !path.startsWith("/static/");
        }
        return bool;
    }

}
