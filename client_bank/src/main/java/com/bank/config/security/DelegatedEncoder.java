package com.bank.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashMap;

import static org.springframework.security.crypto.password.Pbkdf2PasswordEncoder.SecretKeyFactoryAlgorithm.PBKDF2WithHmacSHA256;

@Configuration
public class DelegatedEncoder {
    private final static String ALGORITHM = "pbkdf2";
    private final static String SECRET = "g7)*6_DA";
    private static final int SALT_LENGTH = 16; // 16 bytes (128 bits) salt length
    private static final int HASH_LENGTH = 32;
    private static final int PARALLELISM = 1;
    private static final int MEMORY = 65536;
    private static final int ITERATIONS = 10000;
    private static final int CPU_COST = 16384;
    private static final int MEMORY_COST = 8;
    private static final int KEY_LENGTH = 64;
    @Bean
    public static PasswordEncoder build() {
        return new DelegatingPasswordEncoder(ALGORITHM,
                new HashMap<String, PasswordEncoder>(11) {{
                    put("pbkdf2", new org.springframework.security.crypto.password.Pbkdf2PasswordEncoder(SECRET,SALT_LENGTH,ITERATIONS,PBKDF2WithHmacSHA256));
                    put("bcrypt", new org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder());
                    put("ldap", new org.springframework.security.crypto.password.LdapShaPasswordEncoder());
                    put("MD4", new org.springframework.security.crypto.password.Md4PasswordEncoder());
                    put("MD5", new org.springframework.security.crypto.password.MessageDigestPasswordEncoder("MD5"));
                    put("noop", org.springframework.security.crypto.password.NoOpPasswordEncoder.getInstance());
                    put("scrypt", new org.springframework.security.crypto.scrypt.SCryptPasswordEncoder(CPU_COST, MEMORY_COST, PARALLELISM, KEY_LENGTH, SALT_LENGTH));
                    put("SHA-1", new org.springframework.security.crypto.password.MessageDigestPasswordEncoder("SHA-1"));
                    put("SHA-256", new org.springframework.security.crypto.password.MessageDigestPasswordEncoder("SHA-256"));
                    put("sha256", new org.springframework.security.crypto.password.StandardPasswordEncoder());
                    put("argon2", new org.springframework.security.crypto.argon2.Argon2PasswordEncoder(SALT_LENGTH, HASH_LENGTH, PARALLELISM, MEMORY, ITERATIONS));
                }}
        );
    }
}
