package com.bank.config.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Log4j2
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {
    private final JwtFilter jwtFilter;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        http.csrf(AbstractHttpConfigurer::disable);
        http
                .authorizeHttpRequests(authorize -> authorize
                        .requestMatchers(AntPathRequestMatcher.antMatcher("/auth/token"),
                                         AntPathRequestMatcher.antMatcher("/users/login"),
                                         AntPathRequestMatcher.antMatcher("/static/**"),
                                         AntPathRequestMatcher.antMatcher("/*"),
                                         AntPathRequestMatcher.antMatcher("/ws/**")).permitAll() //пропускаємо сокет без автентифікації
                        .requestMatchers(AntPathRequestMatcher.antMatcher("/user/**")).hasAnyRole("ADMIN", "USER")
                        .requestMatchers(AntPathRequestMatcher.antMatcher("/adm/**"),
                                         AntPathRequestMatcher.antMatcher(HttpMethod.PUT,"/api/**"),
                                         AntPathRequestMatcher.antMatcher(HttpMethod.POST, "/api/**"),
                                         AntPathRequestMatcher.antMatcher(HttpMethod.DELETE,"/api/**")).hasRole("ADMIN")
                        .anyRequest().authenticated()
                )
                .formLogin(formLogin -> formLogin
                        .loginPage("/users/login")
                        .permitAll()
                )
                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }
}
