package com.bank.config.security;

import com.bank.model.Customer;
import com.bank.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Arrays;

@Log4j2
@Configuration
@RequiredArgsConstructor
public class CustomerDetailsServiceJPA implements UserDetailsService{
    private final CustomerService service;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("-------------- loading user: {}", username);
        return service.findByName(username)
                .map(this::mapper)
                .orElseThrow(() -> new UsernameNotFoundException(
                        String.format("user %s not found", username)
                ));
    }

    private UserDetails mapper(Customer entry) {
        return User
                .withUsername(entry.getName())
                .password(entry.getPassword())
                .roles(Arrays.stream(entry.getRoles())
                        .map(role -> "ROLE_" + role)
                        .toArray(String[]::new))
                .build();
    }
}