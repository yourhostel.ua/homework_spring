package com.bank.config.web;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.PathResourceResolver;

import java.io.IOException;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/api/**")
                .allowedOrigins("http://localhost:3000")
                .allowedMethods("GET", "POST", "PUT", "DELETE")
                .allowedHeaders("*")
                .allowCredentials(true);
    }

    /**
     * 1. registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
     * Тут ми реєструємо обробник для URL, які починаються з /static/.
     * Такі URL будуть обслуговуватися з директорії classpath:/static/.
     * Це означає, що всі статичні файли, розміщені в директорії src/main/resources/static,
     * будуть доступні за URL-адресою /static/....
     * 2. registry.addResourceHandler("/**")
     * Тут ми реєструємо обробник для решти URL, які не починаються з /static/.
     * Це означає, що всі запити, які не були опрацьовані попереднім обробником, потраплятимуть сюди.
     * 3. .addResourceLocations("classpath:/static/")
     * Тут ми вказуємо, що решта URL, які не починаються з /static/, будуть також обслуговуватися з директорії
     * classpath:/static/. Це зроблено для того, щоб всі інші URL-адреси, які не належать до статичних ресурсів
     * (наприклад, маршрути React додатку), були також оброблені з директорії src/main/resources/static.
     * 4. .resourceChain(false)
     * Це опція, яка відключає ланцюжок ресурсів, який зазвичай застосовується для обробки статичних ресурсів.
     * Коли ресурс Chain встановлено в false, обробка статичних ресурсів відбувається без використання ланцюжка
     * ресурсів. Це дозволяє обробляти статичні ресурси без додаткових перевірок та перенаправлень,
     * що може спричинити прискорення завантаження таких ресурсів.
     * 5. .addResolver(new PathResourceResolver() { ... })
     * Тут додаємо кастомний PathResourceResolver, який обробляє всі інші URL-адреси, які не були оброблені
     * попередніми обробниками. Коли надходить запит на таку URL-адресу, цей PathResourceResolver буде шукати
     * файл у директорії classpath:/static/. Якщо файл знайдено, його буде віддано у відповідь.
     * Якщо файл не знайдено, він перенаправлятиме запит на index.html у директорії classpath:/static/.
     * Це дозволяє реалізувати підтримку вкладених шляхів у React додатку, тому що всі запити на неіснуючі ресурси
     * будуть перенаправлені на index.html, де React додаток зможе опрацювати цей маршрут.
     */

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**")
                .addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/static/")
                .resourceChain(false)
                .addResolver(new PathResourceResolver() {
                    @Override
                    protected Resource getResource(String resourcePath, Resource location) throws IOException {
                        Resource requestedResource = location.createRelative(resourcePath);
                        return requestedResource.exists() && requestedResource.isReadable() ? requestedResource
                                : new ClassPathResource("/static/index.html");
                    }
                });
    }

}
