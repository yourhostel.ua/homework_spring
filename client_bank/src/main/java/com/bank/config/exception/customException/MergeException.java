package com.bank.config.exception.customException;

public class MergeException extends RuntimeException{
    private final String code;

    public MergeException(String message, String code) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
