package com.bank.config.exception.customException;

public class CreationException extends RuntimeException {

    public CreationException(String message) {
        super(message);
    }
}
