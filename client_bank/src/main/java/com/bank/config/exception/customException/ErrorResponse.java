package com.bank.config.exception.customException;

public record ErrorResponse(String message, String code) {}
