package com.bank.config.exception;

import com.bank.config.exception.customException.CreationException;
import com.bank.config.exception.customException.ErrorResponse;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@Log4j2
@RestControllerAdvice
public class GlobalExceptionHandler {
    /**
     * Oбробляємо глобально всі ендпоінти з EntityNotFoundException
     */
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleEntityNotFoundException(EntityNotFoundException ex) {
        HttpStatus nf = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(new ErrorResponse(ex.getMessage() + " not found",
                nf.name()), nf);
    }

    /**
     * Oбробляємо глобально всі ендпоінти з MethodArgumentNotValidException
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, String> handleValidationException(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult()
                .getFieldErrors()
                .forEach(error -> errors.put(error.getField(), error.getDefaultMessage()));
        return errors;
    }

    /**
     * Необроблені винятки
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
        HttpStatus ise = HttpStatus.INTERNAL_SERVER_ERROR;
        return new ResponseEntity<>(new ErrorResponse("Internal Server Error" + ex.getMessage(),
                ise.name()), ise);
    }

    /**
     * Обробляємо користувальницькі винятки
     */
    @ExceptionHandler(CreationException.class)
    public ResponseEntity<ErrorResponse> handleCreationException(CreationException ex) {
        HttpStatus ue = HttpStatus.UNPROCESSABLE_ENTITY;
        return new ResponseEntity<>(new ErrorResponse(ex.getMessage(),
                ue.name()), ue);
    }
}
