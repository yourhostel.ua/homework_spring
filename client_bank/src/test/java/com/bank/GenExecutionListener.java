package com.bank;

import com.bank.utils.Gen;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;

public class GenExecutionListener extends AbstractTestExecutionListener {
    //перевизначаємо метод для виконання генерації перед запуском усіх тестів
    @Override
    public void beforeTestClass(TestContext testContext) {
        ApplicationContext context = testContext.getApplicationContext();
        Gen gen = Gen.of(context);
        gen.genCustomers();
        gen.genAccounts();
        gen.genEmployer();
    }
}
