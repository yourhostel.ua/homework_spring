package com.bank;

import lombok.Data;

@Data
public class TestLoginResponse {
    private Boolean status;
    private String error;
    private String token;
    private String[] role;
}
