package com.bank;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TestUpdateRequest {
    private String name;
    private String email;
    private String tel;
    private Integer age;
}
