package com.bank;

import com.bank.dto.customer.CustomerRequest;
import com.bank.dto.customer.LoginRequest;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
@TestExecutionListeners(listeners = {
        //дата генератор
        GenExecutionListener.class,
        //слухач для інжекту залежності у тестовий клас
        DependencyInjectionTestExecutionListener.class,
})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class IntegrationApiCustomerTests {
    private final String baseUrl = "http://localhost:9000/";
    private final RestTemplate restTemplate = new RestTemplate();
    private final Properties properties = new Properties();
    private TestLoginResponse loginResponse;
    private  HttpHeaders authHeaders;

    @BeforeEach
    public void setup(){
        //отримуємо доступ до згенерованого адміністратора
        LoginRequest loginRequest = new LoginRequest("Greak", "568D!6s", false);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        //отримуємо об'єкт відповіді з токеном
        ResponseEntity<TestLoginResponse> response = restTemplate.exchange(
                baseUrl + "/auth/token",
                HttpMethod.POST,
                new HttpEntity<>(loginRequest, headers),
                TestLoginResponse.class
        );

        if (response.getStatusCode() == HttpStatus.OK && response.getBody() != null) {
            loginResponse = response.getBody();
        }
        //отримуємо поточний секрет для декодування токена
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("jwt.properties")) {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //робимо авторизаційний загальний хедер з токеном доступу, отриманим від контролера аунтифікації.
        authHeaders = new HttpHeaders();
        authHeaders.setBearerAuth(loginResponse.getToken());

    }

    @Order(1)
    @Test
    public void testStatusField() {
        assertNotNull(loginResponse);
        assertTrue(loginResponse.getStatus());
    }

    @Order(2)
    @Test
    public void testErrorField() {
        assertNotNull(loginResponse);
        assertNull(loginResponse.getError());
    }

    //перевіряємо довжину токена
    @Order(2)
    @Test
    public void testTokenField() {
        assertNotNull(loginResponse.getToken());
        assertTrue(loginResponse.getToken().length() > 10);

    }

    //перевіряємо що ролі додані у правильній послідовності
    //і то що ролі дві
    @Order(3)
    @Test
    public void testRolesField() {
        assertNotNull(loginResponse.getRole());
        assertEquals(2, loginResponse.getRole().length);
        assertEquals("ADMIN", loginResponse.getRole()[0]);
        assertEquals("USER", loginResponse.getRole()[1]);
    }

    @Order(4)
    @Test
    void testJwtTokenFields() {
        Claims claims = Jwts.parser()
                .setSigningKey(properties.getProperty("jwt.secret"))
                .parseClaimsJws(loginResponse.getToken())
                .getBody();
        // перевіряємо, що id юзера дорівнює "1"
        assertEquals("1", claims.getSubject());
        // перевіряємо, що дата створення токена раніше його дати закінчення
        assertTrue(claims.getIssuedAt().before(claims.getExpiration()));

    }

    //ендпоінт-тести
    @Order(5)
    @Test
    public void testGetCustomer() {
        HttpEntity<String> entity = new HttpEntity<>("parameters", authHeaders);
        ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/api/customers/1", HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
    @Order(6)
    @Test
    public void testGetAllCustomers() {
        HttpEntity<String> entity = new HttpEntity<>("parameters", authHeaders);
        ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/api/customers", HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

    }
    @Order(7)
    @Test
    public void testCreateCustomer() {
        CustomerRequest customerRequest = new CustomerRequest("John","johny@ahol.gyk","+38(065)3716888",20,"Test3*152");
        HttpHeaders headers = authHeaders;
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(loginResponse.getToken());
        HttpEntity<CustomerRequest> createEntity = new HttpEntity<>(customerRequest, headers);

        ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/api/customers/create", HttpMethod.POST, createEntity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());


    }
    @Order(8)
    @Test
    public void testUpdateCustomer() {
        Long customerIdToUpdate = 2L;
        TestUpdateRequest customerUpdateRequest = new TestUpdateRequest("John","johny@ahol.gyk","+38(065)3716888",20);
        HttpHeaders headers = authHeaders;
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(loginResponse.getToken());
        HttpEntity<TestUpdateRequest> updateEntity = new HttpEntity<>(customerUpdateRequest, headers);

        ResponseEntity<String> response = restTemplate
                .exchange(String.format("%s/api/customers/update/%s",
                        baseUrl, customerIdToUpdate), HttpMethod.PUT, updateEntity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        System.out.println(response.getBody());

    }
}






