# homework_spring



## Getting started

- Для початку додаємо Maven.
- Потім треба запустити інсталяцію react.
```
cd client_bank/front
npm instal
```
- В папку `front` буде встановлено `node_modules` та залежності.
- Після цього запускаємо білдер `npm run build` його замовчування перевизначено в `package.json`.
```
 "scripts": {
    "build": "react-scripts build && node copy-files.js",
     інші...
  },
```
- Тут в `copy-files.js` створено мапінг для прокидання створених файлів на сервер додатка в resources.

#### Далі створюємо в IDE профілі dev та prod до обох прописуємо змінні середовища: 
- Для `dev` налаштовуємо базу даних H2
```
DB_URL         jdbc:h2:mem:db
DB_USERNAME    sa
DB_PASSWORD    sa
```
- Для `prod` налаштовуємо MySql
```
DB_URL        jdbc:mysql://ваша база даних
DB_USERNAME   user
DB_PASSWORD   password
```
- Якщо ви хочете підключити іншу БД замініть залежність драйвера в POM.xml цю видаліть
```
  <dependency>
    <groupId>com.mysql</groupId>
    <artifactId>mysql-connector-j</artifactId>
  </dependency>
```
- Та додайте свою наприклад postgresql
```
  <dependency>
    <groupId>org.postgreSql</groupId>
    <artifactId>postgresql</artifactId>
  </dependency>
```
#### Генерація даних у БД

-  Для `dev`
- [ ] Просто запускаємо профіль та генерація додасть тестові дані до бази даних Н2;
-  Для `prod`
- [ ] У головному класі `App` створюємо екземпляр Gen з переданим до нього контекстом додатка і викликаємо на ньому методи для генерації відповідних сутностей
```
public class App {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(App.class, args);
            Gen gen = Gen.of(context);
            gen.genCustomers();
            gen.genAccounts();
            gen.genEmployer();
    }
}
```
- [ ] Незалежно від того скільки разів буде запущено генерацію в БД буде створено унікальні Customer Admin і User які можна використовувати для тестування програми. 
```
  role             ADMIN, USER
  name             Greak
  password         568D!6s
  
  role             USER
  name             Bret
  password         S06i7*378
```
- Для `dev` кожна генерація це нові дані і дублювання не буде, тим самим запобігаючи помилці доступу коли існує два однакових користувача. У цьому випадку не можна створити більше 12 користувачів, що обмежено кількістю в `List<Customer> customers` у класі `Gen`.
- Для `prod` можна зробити будь-яку кількість генерацій, але треба враховувати повторення даних (помилка може виникати при спробі отримати одного Customer за ім'ям `Optional<Customer> findByName(String name);`)
- У `test` для генерації задіяні ті ж умови, що й для `dev`.